/*

    Nombre: clinica.sql
    Fecha: 06/04/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: definición de la estructura de datos del sistema de
                 historias clínicas.

    Se crean tablas de auditoria y los triggers porque el control de
    auditoría se hace demasiado complejo por código e introduce
    errores

    Esta tabla es complementaria de la de diagnóstico y utiliza la misma
    tabla de personas debe entonces correrse primero la consulta de
    diagnóstico para recrear las tablas y luego esta.

    Las tablas complementarias que crearemos son:

    Derivacion diccionario de derivaciones de consulta
    ChagasAgudo antecedentes de chagas agudo
    Drogas diccionario de drogas utilizadas en el tratamiento
    Adversos diccionario de efectos adversos
    Tratamiento también, debe incluir droga
    Antecedentes antecedentes de la anamnesis
    Familiares antecedentes familiares
    Sintomas sintomas del paciente
    Disautonia otros síntomas del paciente
    Digestivo tipo de compromiso digestivo
    Examen examen físico del paciente
    Cardiovascular examen del aparato cardiovascular
    Rx evaluación de las radiografías
    Electro resultados del electrocardiograma
    Eco resultados del ecocardiograma
    Holter resultados del holter
    Ergometría resultados de la ergometría
    Clasificacion clasificación de la enfermedad según kuscknir

*/

-- Establecemos la página de códigos
SET CHARACTER_SET_CLIENT=utf8;
SET CHARACTER_SET_RESULTS=utf8;
SET COLLATION_CONNECTION=utf8_spanish_ci;

-- seleccionamos
USE diagnostico;


/***************************************************************************/
/*                                                                         */
/*                       Tipos de Derivación                               */
/*                                                                         */
/***************************************************************************/

/*

    Esta tabla es el diccionario de derivaciones su edición quedará
    restringida a los usuarios del nivel central, no tiene auditoría
    id entero pequeño, clave del registro
    derivacion texto descripción del motivo
    usuario entero clave del usuario
    fecha_alta date fecha de alta del registro

*/

-- eliminamos la tabla de motivos si existe
DROP TABLE IF EXISTS derivacion;

-- la recreamos
CREATE TABLE derivacion (
    id tinyint(1) UNSIGNED NOT NULL AUTO_INCREMENT,
    derivacion varchar(100) NOT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Tipos de derivación';

-- insertamos los valores iniciales
INSERT INTO derivacion (derivacion, usuario) VALUES ("Institución", 1);
INSERT INTO derivacion (derivacion, usuario) VALUES ("Profesional", 1);
INSERT INTO derivacion (derivacion, usuario) VALUES ("Personal", 1);
INSERT INTO derivacion (derivacion, usuario) VALUES ("Familiar", 1);


/***************************************************************************/
/*                                                                         */
/*                              Chagas Agudo                               */
/*                                                                         */
/***************************************************************************/

/*

    Esta tabla contiene los antecedentes de chagas agudo, solo podrá ser
    editada por administradores del nivel central, debería haber un solo
    registro por cada paciente

    id int(6) clave del registro
    paciente int(6) clave del paciente
    sintomas si ha tenido síntomas y signos anteriormente
    observaciones texto, observaciones del usuario
    usuario clave del usuario
    fecha date, fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS agudo;

-- la recreamos
CREATE TABLE agudo (
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    paciente int(6) UNSIGNED NOT NULL,
    sintomas tinyint(1) UNSIGNED DEFAULT 0,
    observaciones mediumtext,
    usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY paciente(paciente),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Datos de chagas agudo';

-- eliminamos la tabla de auditoría si existe
DROP TABLE IF EXISTS auditoria_agudo;

-- la recreamos
CREATE TABLE auditoria_agudo (
    id int(6) UNSIGNED NOT NULL,
    paciente int(6) UNSIGNED NOT NULL,
    sintomas tinyint(1) UNSIGNED DEFAULT 0,
    observaciones mediumtext,
    usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum ("Edicion", "Eliminacion"),
    KEY id(id),
    KEY paciente(paciente),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditorpia de chagas agudo';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_agudo;

-- lo recreamos
CREATE TRIGGER edicion_agudo
AFTER UPDATE ON agudo
FOR EACH ROW
INSERT INTO auditoria_agudo
    (id,
     paciente,
     sintomas,
     observaciones,
     usuario,
     fecha,
     evento)
    VALUES
    (OLD.id,
     OLD.paciente,
     OLD.observaciones,
     OLD.usuario,
     OLD.fecha,
     "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_agudo;

-- lo recreamos
CREATE TRIGGER eliminacion_agudo
AFTER DELETE ON agudo
FOR EACH ROW
INSERT INTO auditoria_agudo
    (id,
     paciente,
     sintomas,
     observaciones,
     usuario,
     fecha,
     evento)
    VALUES
    (OLD.id,
     OLD.paciente,
     OLD.observaciones,
     OLD.usuario,
     OLD.fecha,
     "Eliminacion");

-- eliminamos la vista si existe
DROP VIEW IF EXISTS v_agudo;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_agudo AS
       SELECT diagnostico.agudo.id AS id,
              diagnostico.agudo.paciente AS paciente,
              diagnostico.agudo.sintomas AS sintomas,
              diagnostico.agudo.usuario AS idusuario,
              cce.responsables.usuario As usuario,
              DATE_FORMAT(diagnostico.agudo.fecha, '%d/%m/%Y') AS fecha
       FROM diagnostico.agudo INNER JOIN cce.responsables ON diagnostico.agudo.usuario = cce.responsables.id;


/***************************************************************************/
/*                                                                         */
/*                              Drogas                                     */
/*                                                                         */
/***************************************************************************/

/*

    Diccionario de drogas utilizadas para el tratamiento

    id int clave del registro
    droga varchar nombre de la droga
    dosis int dosis en miligramos
    comercial nombre comercial del medicamento
    usuario entero clave del usuario
    fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS drogas;

-- la recreamos
CREATE TABLE drogas (
    id int(2) UNSIGNED NOT NULL AUTO_INCREMENT,
    droga varchar(200) DEFAULT NULL,
    dosis int(3) UNSIGNED NOT NULL,
    comercial varchar(200) NOT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Diccionario de drogas utilizadas';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_drogas;

-- la recreamos
CREATE TABLE auditoria_drogas (
    id int(2) UNSIGNED NOT NULL,
    droga varchar(200) DEFAULT NULL,
    dosis int(3) UNSIGNED NOT NULL,
    comercial varchar(200) NOT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de drogas utilizadas';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_drogras;

-- lo recreamos
CREATE TRIGGER edicion_drogas
AFTER UPDATE ON drogas
FOR EACH ROW
INSERT INTO auditoria_drogas
       (id,
        droga,
        dosis,
        comercial,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.droga,
        OLD.dosis,
        OLD.comercial,
        OLD.usuario,
        OLD.fecha,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_drogas;

-- lo recreamos
CREATE TRIGGER eliminacion_drogas
AFTER DELETE ON drogas
FOR EACH ROW
INSERT INTO auditoria_drogas
       (id,
        droga,
        dosis,
        comercial,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.droga,
        OLD.dosis,
        OLD.comercial,
        OLD.usuario,
        OLD.fecha,
        "Eliminacion");

-- eliminamos la vista si existe
DROP VIEW IF EXISTS v_drogas;

-- creamos la vista
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_drogas AS
       SELECT diagnostico.drogas.id AS id,
              diagnostico.drogas.droga AS droga,
              diagnostico.drogas.dosis AS dosis,
              diagnostico.drogas.comercial AS comercial,
              diagnostico.drogas.usuario AS idusuario,
              cce.responsables.usuario AS usuario,
              DATE_FORMAT(diagnostico.drogas.fecha, '%d/%m/%y') AS fecha
       FROM diagnostico.drogas INNER JOIN cce.responsables ON diagnostico.drogas.usuario = cce.responsables.id;


/***************************************************************************/
/*                                                                         */
/*                          Efectos Adversos                               */
/*                                                                         */
/***************************************************************************/

/*

    Diccionario de efectos adversos al tratamiento

    id clave del registro
    descripcion texto descripción del efecto
    usuario entero clave del registro
    fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS adversos;

-- la recreamos
CREATE TABLE adversos (
    id int(2) UNSIGNED NOT NULL AUTO_INCREMENT,
    descripcion varchar(250) NOT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Diccionario de efectos adversos';

-- eliminamos la vista
DROP VIEW IF EXISTS v_adversos;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_adversos AS
       SELECT diagnostico.adversos.id AS id,
              diagnostico.adversos.descripcion AS descripcion,
              diagnostico.adversos.usuario AS idusuario,
              cce.responsables.usuario AS usuario,
              DATE_FORMAT(diagnostico.adversos.fecha, '%d/%m/%Y') AS fecha
       FROM diagnostico.adversos INNER JOIN cce.responsables ON diagnostico.adversos.usuario = cce.responsables.id;


/***************************************************************************/
/*                                                                         */
/*                              Tratamiento                                */
/*                                                                         */
/***************************************************************************/

/*

    Esta tabla contiene el esquema de tratamiento de cada paciente, puede
    tener varias entradas por cada paciente

    id clave del registro
    paciente clave del paciente
    inicio date fecha de inicio del tratamiento
    fin date fecha de finalización del tratamiento
    usuario entero clave del usuario
    fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS tratamiento;

-- la recreamos
CREATE TABLE tratamiento (
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    paciente int(6) UNSIGNED NOT NULL,
    inicio date NOT NULL,
    fin date NOT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    KEY paciente(paciente),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Datos del tratamiento';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_tratamiento;

-- la recreamos
CREATE TABLE auditoria_tratamiento (
    id int(6) UNSIGNED NOT NULL,
    paciente int(6) UNSIGNED NOT NULL,
    inicio date NOT NULL,
    fin date NOT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY paciente(paciente),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría del tratamiento';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_tratamiento;

-- lo recreamos
CREATE TRIGGER edicion_tratamiento
AFTER UPDATE ON tratamiento
FOR EACH ROW
INSERT INTO auditoria_tratamiento
            (id,
             paciente,
             inicio,
             fin,
             usuario,
             fecha,
             evento)
            VALUES
            (OLD.id,
             OLD.paciente,
             OLD.inicio,
             OLD.fin,
             OLD.usuario,
             OLD.fecha,
             'Edicion');

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_tratamiento;

-- lo recreamos
CREATE TRIGGER eliminacion_tratamiento
AFTER DELETE ON tratamiento
FOR EACH ROW
INSERT INTO auditoria_tratamiento
            (id,
             paciente,
             inicio,
             fin,
             usuario,
             fecha,
             evento)
            VALUES
            (OLD.id,
             OLD.paciente,
             OLD.inicio,
             OLD.fin,
             OLD.usuario,
             OLD.fecha,
             'Eliminacion');

-- eliminamos la vista
DROP VIEW IF EXISTS v_tratamiento;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_tratamiento AS
       SELECT diagnostico.tratamiento.id AS id,
              diagnostico.tratamiento.paciente AS paciente,
              DATE_FORMAT(diagnostico.tratamiento.inicio, '%d/%m/%Y') AS inicio,
              DATE_FORMAT(diagnostico.tratamiento.fin, '%d/%m/%Y') AS fin,
              diagnostico.tratamiento.usuario AS idusuario,
              cce.responsables.usuario AS usuario,
              DATE_FORMAT(diagnostico.tratamiento.fecha, '%d/%m/%Y') AS fecha
       FROM diagnostico.tratamiento INNER JOIN cce.responsables ON diagnostico.tratamiento.usuario = cce.responsables.id;


/***************************************************************************/
/*                                                                         */
/*                              Antecedentes                               */
/*                                                                         */
/***************************************************************************/

/*

    Tabla con los antecedentes tóxicos del paciente esta tendría que haber
    un solo registro por cada paciente

    id entero clave del registro
    paciente entero clave del paciente
    fuma entero (0 si no fuma)
    alcohol entero (o si no toma)
    bebida enum tipo de bebida
    adicciones texto descripción de las adicciones
    usuario entero clave del usuario
    fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS antecedentes;

-- la recreamos
CREATE TABLE antecedentes(
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    paciente int(6) UNSIGNED NOT NULL,
    fuma tinyint(1) UNSIGNED DEFAULT 0,
    alcohol tinyint(1) UNSIGNED DEFAULT 0,
    bebida enum('Fermentada', 'Vino', 'Destilada', 'Otra') DEFAULT NULL,
    adicciones text DEFAULT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY paciente(paciente),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Antecedentes del paciente';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_antecedentes;

-- la recreamos
CREATE TABLE auditoria_antecedentes(
    id int(6) UNSIGNED NOT NULL,
    paciente int(6) UNSIGNED NOT NULL,
    fuma tinyint(1) UNSIGNED DEFAULT 0,
    alcohol tinyint(1) UNSIGNED DEFAULT 0,
    bebida enum('Fermentada', 'Vino', 'Destilada', 'Otra') DEFAULT NULL,
    adicciones text DEFAULT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY paciente(paciente),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de antecedentes del paciente';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_antecedentes;

-- lo recreamos
CREATE TRIGGER edicion_antecedentes
AFTER UPDATE ON antecedentes
FOR EACH ROW
INSERT INTO auditoria_antecedentes
       (id,
        paciente,
        fuma,
        alcohol,
        bebida,
        adicciones,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.fuma,
        OLD.alcohol,
        OLD.bebida,
        OLD.adicciones,
        OLD.usuario,
        OLD.fecha,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_antecedentes;

-- lo recreamos
CREATE TRIGGER eliminacion_antecedentes
AFTER DELETE ON antecedentes
FOR EACH ROW
INSERT INTO auditoria_antecedentes
       (id,
        paciente,
        fuma,
        alcohol,
        bebida,
        adicciones,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.fuma,
        OLD.alcohol,
        OLD.bebida,
        OLD.adicciones,
        OLD.usuario,
        OLD.fecha,
        "Eliminacion");

-- eliminamos la vista
DROP VIEW IF EXISTS v_antecedentes;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_antecedentes AS
       SELECT diagnostico.antecedentes.id AS id,
              diagnostico.antecedentes.paciente AS paciente,
              diagnostico.antecedentes.fuma AS fuma,
              diagnostico.antecedentes.alcohol AS alcohol,
              diagnostico.antecedentes.bebida AS bebida,
              diagnostico.antecedentes.adicciones AS adicciones,
              diagnostico.antecedentes.usuario AS idusuario,
              cce.responsables.usuario AS usuario,
              DATE_FORMAT(diagnostico.antecedentes.fecha, '%d/%m/%Y') AS fecha
       FROM diagnostico.antecedentes INNER JOIN cce.responsables ON diagnostico.antecedentes.usuario = cce.responsables.id;


/***************************************************************************/
/*                                                                         */
/*                              Familiares                                 */
/*                                                                         */
/***************************************************************************/

/*

    Tabla con los antecedentes familiares de cada paciente, esta tendría
    que haber un solo registro por cada paciente

    id entero clave del registro
    paciente entero clave del paciente
    subita tinyint si hay muerte súbita (0 falso - 1 verdadero)
    cardiopatia tinyint si hay cardiopatía (0 falso - 1 verdadero)
    disfagia tinyint si ha disfagia, constipación o evidencia de megas
    nosabe tiniynt (0 falso - 1 verdadero)
    notiene tinyint (0 falso - 1 verdadero)
    otra texto descripción de la enfermedad
    usuario entero clave del usuario
    fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS familiares;

-- la recreamos
CREATE TABLE familiares (
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    paciente int(6) UNSIGNED NOT NULL,
    subita tinyint(1) UNSIGNED DEFAULT 0,
    cardiopatia tinyint(1) UNSIGNED DEFAULT 0,
    disfagia tinyint(1) UNSIGNED DEFAULT 0,
    nosabe tinyint(1) UNSIGNED DEFAULT 0,
    notiene tinyint(1) UNSIGNED DEFAULT 0,
    otra text DEFAULT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY paciente(paciente),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Antecedentes familiares';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_familiares;

-- la recreamos
CREATE TABLE auditoria_familiares (
    id int(6) UNSIGNED NOT NULL,
    paciente int(6) UNSIGNED NOT NULL,
    subita tinyint(1) UNSIGNED DEFAULT 0,
    cardiopatia tinyint(1) UNSIGNED DEFAULT 0,
    disfagia tinyint(1) UNSIGNED DEFAULT 0,
    nosabe tinyint(1) UNSIGNED DEFAULT 0,
    notiene tinyint(1) UNSIGNED DEFAULT 0,
    otra text DEFAULT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY paciente(paciente),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de antecedentes familiares';

-- eliminamos el trigger
DROP TRIGGER IF EXISTS edicion_familiares;

-- lo recreamos
CREATE TRIGGER edicion_familiares
AFTER UPDATE ON familiares
FOR EACH ROW
INSERT INTO auditoria_familiares
       (id,
        paciente,
        subita,
        cardiopatia,
        disfagia,
        nosabe,
        notiene,
        otra,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.subita,
        OLD.cardiopatia,
        OLD.disfagia,
        OLD.nosabe,
        OLD.notiene,
        OLD.otra,
        OLD.usuario,
        OLD.fecha,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_familiares;

-- lo recreamos
CREATE TRIGGER eliminacion_familiares
AFTER DELETE ON familiares
FOR EACH ROW
INSERT INTO auditoria_familiares
       (id,
        paciente,
        subita,
        cardiopatia,
        disfagia,
        nosabe,
        notiene,
        otra,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.subita,
        OLD.cardiopatia,
        OLD.disfagia,
        OLD.nosabe,
        OLD.notiene,
        OLD.otra,
        OLD.usuario,
        OLD.fecha,
        "Eliminacion");

-- eliminamos la vista
DROP VIEW IF EXISTS v_familiares;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_familiares AS
       SELECT diagnostico.familiares.id AS id,
              diagnostico.familiares.paciente AS paciente,
              diagnostico.familiares.subita As subita,
              diagnostico.familiares.cardiopatia As cardiopatia,
              diagnostico.familiares.disfagia AS disfagia,
              diagnostico.familiares.nosabe AS nosabe,
              diagnostico.familiares.notiene AS notiene,
              diagnostico.familiares.otra AS otra,
              diagnostico.familiares.usuario AS idusuario,
              cce.responsables.usuario AS usuario,
              DATE_FORMAT(diagnostico.familiares.fecha, '%d/%m/%Y') AS fecha
       FROM diagnostico.familiares INNER JOIN cce.responsables ON diagnostico.familiares.usuario = cce.responsables.id;


/***************************************************************************/
/*                                                                         */
/*                                 Síntomas                                */
/*                                                                         */
/***************************************************************************/

/*

    Tabla con los síntomas del paciente, esta tabla debería tener una entrada
    por cada paciente

    id entero clave del registro
    paciente entero clave del paciente
    disnea enum tipo de disnea
    palpitaciones enum tipo de palpitaciones
    precordial enum si tiene dolor precordial
    conciencia enum si ha perdido la consciencia
    presincope enum si ha tenido presíncope
    edema enum si tiene edema de miembros inferiores
    observaciones texto observaciones del usuario
    usuario entero clave del registro
    fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS sintomas;

-- la recreamos
CREATE TABLE sintomas (
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    paciente int(6) UNSIGNED NOT NULL,
    disnea enum('Esfuerzos Leves', 'Esfuerzos Moderados', 'Grandes Esfuerzos', 'En Reposo', 'DPN', 'No Tiene') DEFAULT 'No Tiene',
    palpitaciones enum('Raramente', 'Frecuentes', 'No Tiene') DEFAULT 'No Tiene',
    precordial enum('Tipico', 'Atipico', 'No Tiene') DEFAULT 'No Tiene',
    conciencia enum('Si', 'No') DEFAULT 'No',
    presincope enum('Si', 'No') DEFAULT 'No',
    edema enum('Si', 'No') DEFAULT 'No',
    observaciones text DEFAULT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY paciente(paciente),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Síntomas del paciente';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_sintomas;

-- la recreamos
CREATE TABLE auditoria_sintomas (
    id int(6) UNSIGNED NOT NULL,
    paciente int(6) UNSIGNED NOT NULL,
    disnea enum('Esfuerzos Leves', 'Esfuerzos Moderados', 'Grandes Esfuerzos', 'En Reposo', 'DPN', 'No Tiene') DEFAULT 'No Tiene',
    palpitaciones enum('Raramente', 'Frecuentes', 'No Tiene') DEFAULT 'No Tiene',
    precordial enum('Tipico', 'Atipico', 'No Tiene') DEFAULT 'No Tiene',
    conciencia enum('Si', 'No') DEFAULT 'No',
    presincope enum('Si', 'No') DEFAULT 'No',
    edema enum('Si', 'No') DEFAULT 'No',
    observaciones text DEFAULT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum('Edicion', 'Eliminacion'),
    KEY id(id),
    KEY paciente(paciente),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de síntomas del paciente';

-- eliminamos el trigger
DROP TRIGGER IF EXISTS edicion_sintomas;

-- lo recreamos
CREATE TRIGGER edicion_sintomas
AFTER UPDATE ON sintomas
FOR EACH ROW
INSERT INTO auditoria_sintomas
       (id,
        paciente,
        disnea,
        palpitaciones,
        precordial,
        conciencia,
        presincope,
        edema,
        observaciones,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.disnea,
        OLD.palpitaciones,
        OLD.precordial,
        OLD.conciencia,
        OLD.presincope,
        OLD.edema,
        OLD.observaciones,
        OLD.usuario,
        OLD.fecha,
        'Edicion');

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_sintomas;

-- lo recreamos
CREATE TRIGGER eliminacion_sintomas
AFTER DELETE ON sintomas
FOR EACH ROW
INSERT INTO auditoria_sintomas
       (id,
        paciente,
        disnea,
        palpitaciones,
        precordial,
        conciencia,
        presincope,
        edema,
        observaciones,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.disnea,
        OLD.palpitaciones,
        OLD.precordial,
        OLD.conciencia,
        OLD.presincope,
        OLD.edema,
        OLD.observaciones,
        OLD.usuario,
        OLD.fecha,
        'Eliminacion');

-- eliminamos la vista
DROP VIEW IF EXISTS v_sintomas;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_sintomas AS
       SELECT diagnostico.sintomas.id AS id,
              diagnostico.sintomas.paciente AS paciente,
              diagnostico.sintomas.disnea AS disnea,
              diagnostico.sintomas.palpitaciones AS palpitaciones,
              diagnostico.sintomas.precordial AS precordial,
              diagnostico.sintomas.conciencia AS conciencia,
              diagnostico.sintomas.presincope AS presincope,
              diagnostico.sintomas.edema AS edema,
              diagnostico.sintomas.observaciones AS observaciones,
              diagnostico.sintomas.usuario AS idusuario,
              cce.responsables.usuario AS usuario,
              DATE_FORMAT(diagnostico.sintomas.fecha, '%d/%m/%Y') AS fecha
       FROM diagnostico.sintomas INNER JOIN cce.responsables ON diagnostico.sintomas.usuario = cce.responsables.id;


/***************************************************************************/
/*                                                                         */
/*                              Disautonia                                 */
/*                                                                         */
/***************************************************************************/

/*

    Tabla con los datos de disautonía del paciente, esta también debería
    haber un solo registro por cada paciente

    id entero clave del registro
    paciente entero clave del paciente
    hipotensión tinyint si tiene hipotensión (0 falso - 1 verdadero)
    bradicardia tinyint (0 falso - 1 verdadero)
    astenia tinyint (0 falso - 1 verdadero)
    hipoastefati tinyint (0 falso - 1 verdadero) (hipotensión astenia y fatigabilidad)
    bradiastefati tinyint (0 falso - 1 verdadero) (bradicardia astenia y fatigabilidad)
    notiene tinyint (0 falso - 1 verdadero)
    usuario entero clave del usuario
    fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS disautonia;

-- la recreamos
CREATE TABLE disautonia(
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    paciente int(6) UNSIGNED NOT NULL,
    hipotension tinyint(1) UNSIGNED DEFAULT 0,
    bradicardia tinyint(1) UNSIGNED DEFAULT 0,
    astenia tinyint(1) UNSIGNED DEFAULT 0,
    hipoastefati tinyint(1) UNSIGNED DEFAULT 0,
    bradiastefati tinyint(1) UNSIGNED DEFAULT 0,
    notiene tinyint(1) UNSIGNED DEFAULT 0,
    usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY paciente(paciente),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Datos de disautonía';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_disautonia;

-- la recreamos
CREATE TABLE auditoria_disautonia(
    id int(6) UNSIGNED NOT NULL,
    paciente int(6) UNSIGNED NOT NULL,
    hipotension tinyint(1) UNSIGNED DEFAULT 0,
    bradicardia tinyint(1) UNSIGNED DEFAULT 0,
    astenia tinyint(1) UNSIGNED DEFAULT 0,
    hipoastefati tinyint(1) UNSIGNED DEFAULT 0,
    bradiastefati tinyint(1) UNSIGNED DEFAULT 0,
    notiene tinyint(1) UNSIGNED DEFAULT 0,
    usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY paciente(paciente),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Datos de disautonía';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_disautonia;

-- lo recreamos
CREATE TRIGGER edicion_disautonia
AFTER UPDATE ON disautonia
FOR EACH ROW
INSERT INTO auditoria_disautonia
       (id,
        paciente,
        hipotension,
        bradicardia,
        astenia,
        hipoastefati,
        notiene,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.hipotension,
        OLD.bradicardia,
        OLD.astenia,
        OLD.hipoastefati,
        OLD.notiene,
        OLD.usuario,
        OLD.fecha,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_disautonia;

-- lo recreamos
CREATE TRIGGER eliminacion_disautonia
AFTER DELETE ON disautonia
FOR EACH ROW
INSERT INTO auditoria_disautonia
       (id,
        paciente,
        hipotension,
        bradicardia,
        astenia,
        hipoastefati,
        notiene,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.hipotension,
        OLD.bradicardia,
        OLD.astenia,
        OLD.hipoastefati,
        OLD.notiene,
        OLD.usuario,
        OLD.fecha,
        "Eliminacion");

-- eliminamos la vista
DROP VIEW IF EXISTS v_disautonia;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_disautonia AS
       SELECT diagnostico.disautonia.id AS id,
              diagnostico.disautonia.paciente AS paciente,
              diagnostico.disautonia.hipotension AS hipotension,
              diagnostico.disautonia.bradicardia AS bradicardia,
              diagnostico.disautonia.astenia AS astenia,
              diagnostico.disautonia.hipoastefati AS hipoastefati,
              diagnostico.disautonia.notiene AS notiene,
              diagnostico.disautonia.usuario AS idusuario,
              cce.responsables.usuario AS usuario
       FROM diagnostico.disautonia INNER JOIN cce.responsables ON diagnostico.disautonia.usuario = cce.responsables.id;


/***************************************************************************/
/*                                                                         */
/*                              Digestivo                                  */
/*                                                                         */
/***************************************************************************/

/*

    Tabla con los antecedentes del compromiso digestivo, esta tabla tendría
    una entrada por cada paciente

    id entero clave del registro
    paciente entero clave del paciente
    disfagia tinyint (0 falso - 1 verdadero)
    pirosis tinyint (0 falso - 1 verdadero)
    regurgitacion tinyint (0 falso - 1 verdadero)
    constipacion tinyint (0 falso - 1 verdadero)
    bolo tinyint (0 falso - 1 verdadero)
    notiene tinyint (0 falso - 1 verdadero)
    usuario entero clave del usuario
    fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS digestivo;

-- la recreamos
CREATE TABLE digestivo(
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    paciente int(6) UNSIGNED NOT NULL,
    disfagia tinyint(1) DEFAULT 0,
    pirosis tinyint(1) DEFAULT 0,
    regurgitacion tinyint(1) DEFAULT 0,
    constipacion tinyint(1) DEFAULT 0,
    bolo tinyint(1) DEFAULT 0,
    notiene tinyint(1) DEFAULT 0,
    usuario int(6) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY paciente(paciente),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Datos de compromiso digestivo';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_digestivo;

-- la recreamos
CREATE TABLE auditoria_digestivo(
    id int(6) UNSIGNED NOT NULL,
    paciente int(6) UNSIGNED NOT NULL,
    disfagia tinyint(1) DEFAULT 0,
    pirosis tinyint(1) DEFAULT 0,
    regurgitacion tinyint(1) DEFAULT 0,
    constipacion tinyint(1) DEFAULT 0,
    bolo tinyint(1) DEFAULT 0,
    notiene tinyint(1) DEFAULT 0,
    usuario int(6) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion") NOT NULL,
    KEY id(id),
    KEY paciente(paciente),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría del compromiso digestivo';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_digestivo;

-- lo recreamos
CREATE TRIGGER edicion_digestivo
AFTER UPDATE ON digestivo
FOR EACH ROW
INSERT INTO auditoria_digestivo
       (id,
        paciente,
        disfagia,
        pirosis,
        regurgitacion,
        constipacion,
        bolo,
        notiene,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.disfagia,
        OLD.pirosis,
        OLD.regurgitacion,
        OLD.constipacion,
        OLD.bolo,
        OLD.notiene,
        OLD.usuario,
        OLD.fecha,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_digestivo;

-- lo recreamos
CREATE TRIGGER eliminacion_digestivo
AFTER DELETE ON digestivo
FOR EACH ROW
INSERT INTO auditoria_digestivo
       (id,
        paciente,
        disfagia,
        pirosis,
        regurgitacion,
        constipacion,
        bolo,
        notiene,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.disfagia,
        OLD.pirosis,
        OLD.regurgitacion,
        OLD.constipacion,
        OLD.bolo,
        OLD.notiene,
        OLD.usuario,
        OLD.fecha,
        "Eliminacion");

-- eliminamos la vista
DROP VIEW IF EXISTS v_digestivo;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_digestivo AS
       SELECT diagnostico.digestivo.id AS id,
              diagnostico.digestivo.paciente AS paciente,
              diagnostico.digestivo.disfagia AS disfagia,
              diagnostico.digestivo.pirosis AS pirosis,
              diagnostico.digestivo.regurgitacion AS regurgitacion,
              diagnostico.digestivo.constipacion AS constipacion,
              diagnostico.digestivo.bolo AS bolo,
              diagnostico.digestivo.notiene AS notiene,
              diagnostico.digestivo.usuario AS idusuario,
              cce.responsables.usuario AS usuario,
              DATE_FORMAT(diagnostico.digestivo.fecha, '%d/%m/%Y') AS fecha
       FROM diagnostico.digestivo INNER JOIN cce.responsables ON diagnostico.digestivo.usuario = cce.responsables.id;


/***************************************************************************/
/*                                                                         */
/*                          Examan Físico                                  */
/*                                                                         */
/***************************************************************************/

/*

    Tabla con el resultado del examen físico

    id entero clave del registro
    paciente entero clave del paciente
    tamax entero tensión arterial máxima
    tamin entero tensión arterial mínima
    peso entero peso en kilos
    fc ver unidad de medida
    spo entero saturación de oxígeno en porcentaje
    talla flotante con dos decimales altura
    bmi ver unidad de medida
    edema entero pequeño si hay edema de miembros inferiores
          0 falso - 1 verdadero
    sp entero pequeño 0 falso - 1 verdadero
    mvdisminuido entero pequeño 0 falso - 1 verdadero
    crepitantes entero pequeño 0 falso - 1 verdadero
    sibilancias entero pequeño 0 falso - 1 verdadero

*/


/***************************************************************************/
/*                                                                         */
/*                         Cardiovascular                                  */
/*                                                                         */
/***************************************************************************/

/*

    id entero clave del registro
    paciente entero clave del paciente
    auscultacion enum resultado de la auscultación
    sistolicos enum tipo de soplo sistólico
    diastolico enum tipo de soplo diastólico
    hepatomegalia tinyint (0 falso - 1 verdadero)
    esplenomegalia tinyint (0 falso - 1 verdadero)
    ingurgitacion tinyint (0 falso - 1 verdadero)
    usuario entero clave del usuario
    fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS cardiovascular;

-- la recreamos
CREATE TABLE cardiovascular(
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    paciente int(6) UNSIGNED NOT NULL,
    auscultacion enum("Normal", "Ritmo Irregular", "3er. R", "4to. R") DEFAULT NULL,
    sistolicos enum("Eyectivo Aortico", "Regurgitativo mitral", "No Tiene") DEFAULT NULL,
    diastolicos enum("Aortico", "Mitral", "No Tiene") DEFAULT NULL,
    hepatomegalia tinyint(1) DEFAULT 0,
    esplenomegalia tinyint(1) DEFAULT 0,
    ingurgitacion tinyint(1) DEFAULT 0,
    usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY paciente(paciente),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Datos del aparato cardiovascular';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_cardiovascular;

-- la recreamos
CREATE TABLE auditoria_cardiovascular(
    id int(6) UNSIGNED NOT NULL,
    paciente int(6) UNSIGNED NOT NULL,
    auscultacion enum("Normal", "Ritmo Irregular", "3er. R", "4to. R") DEFAULT NULL,
    sistolicos enum("Eyectivo Aortico", "Regurgitativo mitral", "No Tiene") DEFAULT NULL,
    diastolicos enum("Aortico", "Mitral", "No Tiene") DEFAULT NULL,
    hepatomegalia tinyint(1) DEFAULT 0,
    esplenomegalia tinyint(1) DEFAULT 0,
    ingurgitacion tinyint(1) DEFAULT 0,
    usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY paciente(paciente),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría del aparato cardiovascular';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS auditoria_cardiovascular;

-- lo recreamos
CREATE TRIGGER edicion_cardiovascular
AFTER UPDATE ON cardiovascular
FOR EACH ROW
INSERT INTO auditoria_cardiovascular
       (id,
        paciente,
        auscultacion,
        sistolicos,
        diastolicos,
        hepatomegalia,
        esplenomegalia,
        ingurgitacion,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.auscultacion,
        OLD.sistolicos,
        OLD.diastolicos,
        OLD.hepatomegalia,
        OLD.esplenomegalia,
        OLD.ingurgitacion,
        OLD.usuario,
        OLD.fecha,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_cardivascular;

-- lo recreamos
CREATE TRIGGER eliminacion_cardiovascular
AFTER DELETE ON cardiovascular
FOR EACH ROW
INSERT INTO auditoria_cardiovascular
       (id,
        paciente,
        auscultacion,
        sistolicos,
        diastolicos,
        hepatomegalia,
        esplenomegalia,
        ingurgitacion,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.auscultacion,
        OLD.sistolicos,
        OLD.diastolicos,
        OLD.hepatomegalia,
        OLD.esplenomegalia,
        OLD.ingurgitacion,
        OLD.usuario,
        OLD.fecha,
        "Eliminacion");

-- eliminamos la vista
DROP VIEW IF EXISTS v_cardiovascular;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_cardiovascular AS
       SELECT diagnostico.cardiovascular.id AS id,
              diagnostico.cardiovascular.paciente AS paciente,
              diagnostico.cardiovascular.auscultacion AS auscultacion,
              diagnostico.cardiovascular.sistolicos AS sistolicos,
              diagnostico.cardiovascular.diastolicos As diastolicos,
              diagnostico.cardiovascular.hepatomegalia AS hepatomegalia,
              diagnostico.cardiovascular.esplenomegalia AS esplenomegalia,
              diagnostico.cardiovascular.ingurgitacion AS ingurgitacion,
              diagnostico.cardiovascular.usuario AS idusuario,
              cce.responsables.usuario AS usuario,
              DATE_FORMAT(diagnostico.cardiovascular.fecha, '%d/%m/%Y') AS fecha
       FROM diagnostico.cardiovascular INNER JOIN cce.responsables ON diagnostico.cardiovascular.usuario = cce.responsables.id;


/***************************************************************************/
/*                                                                         */
/*                               Rx                                        */
/*                                                                         */
/***************************************************************************/

/*

    Esta tabla contiene información sobre las radiografías y puede haber
    mas de una entrada por paciente

    id clave del registro
    paciente entero clave del paciente
    fecha date fecha de toma de la radiografía
    itc tinyint (0 falso - 1 verdadero)
    cardiomegalia tinyint (0 falso - 1 verdadero)
    pleuro tinyint (0 falso - 1 verdadero)
    epoc tinyint (0 falso - 1 verdadero)
    calcificaciones tinyint (0 falso - 1 verdadero)
    notiene tinyint (0 falso - 1 verdadero)
    usuario entero clave del usuario
    fecha_alta date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS rx;

-- la recreamos
CREATE TABLE rx (
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    paciente int(6) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    itc tinyint(1) UNSIGNED DEFAULT 0,
    cardiomegalia tinyint(1) UNSIGNED DEFAULT 0,
    pleuro tinyint(1) UNSIGNED DEFAULT 0,
    epoc tinyint(1) UNSIGNED DEFAULT 0,
    calcificaciones tinyint(1) UNSIGNED DEFAULT 0,
    notiene tinyint(1) UNSIGNED DEFAULT 0,
    usuario int(6) UNSIGNED NOT NULL,
    fecha_alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY paciente(paciente),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Información de las radiografías de torax';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_rx;

-- la recreamos
CREATE TABLE auditoria_rx (
    id int(6) UNSIGNED NOT NULL,
    paciente int(6) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    itc tinyint(1) UNSIGNED DEFAULT 0,
    cardiomegalia tinyint(1) UNSIGNED DEFAULT 0,
    pleuro tinyint(1) UNSIGNED DEFAULT 0,
    epoc tinyint(1) UNSIGNED DEFAULT 0,
    calcificaciones tinyint(1) UNSIGNED DEFAULT 0,
    notiene tinyint(1) UNSIGNED DEFAULT 0,
    usuario int(6) UNSIGNED NOT NULL,
    fecha_alta date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY paciente(paciente),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de las radiografías';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_rx;

-- lo recreamos
CREATE TRIGGER edicion_rx
AFTER UPDATE ON rx
FOR EACH ROW
INSERT INTO auditoria_rx
       (id,
        paciente,
        fecha,
        itc,
        cardiomegalia,
        pleuro,
        epoc,
        calcificaciones,
        notiene,
        usuario,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.fecha,
        OLD.itc,
        OLD.cardiomegalia,
        OLD.pleuro,
        OLD.epoc,
        OLD.calcificaciones,
        OLD.notiene,
        OLD.usuario,
        OLD.fecha_alta,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_rx;

-- lo recreamos
CREATE TRIGGER eliminacion_rx
AFTER DELETE ON rx
FOR EACH ROW
INSERT INTO auditoria_rx
       (id,
        paciente,
        fecha,
        itc,
        cardiomegalia,
        pleuro,
        epoc,
        calcificaciones,
        notiene,
        usuario,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.fecha,
        OLD.itc,
        OLD.cardiomegalia,
        OLD.pleuro,
        OLD.epoc,
        OLD.calcificaciones,
        OLD.notiene,
        OLD.usuario,
        OLD.fecha_alta,
        "Eliminacion");

-- eliminamos la vista
DROP VIEW IF EXISTS v_rx;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_rx AS
       SELECT diagnostico.rx.id AS id,
              diagnostico.rx.paciente AS paciente,
              DATE_FORMAT(diagnostico.rx.fecha, '%d/%m/%Y') AS fecha,
              diagnostico.rx.itc AS itc,
              diagnostico.rx.cardiomegalia AS cardiomegalia,
              diagnostico.rx.pleuro AS pleuro,
              diagnostico.rx.epoc AS epoc,
              diagnostico.rx.calcificaciones AS calcificaciones,
              diagnostico.rx.notiene AS notiene,
              diagnostico.rx.usuario AS idusuario,
              cce.responsables.usuario AS usuario,
              DATE_FORMAT(diagnostico.rx.fecha_alta, '%d/%m/%Y') AS fecha_alta
       FROM diagnostico.rx INNER JOIN cce.responsables ON diagnostico.rx.usuario = cce.responsables.id;


/***************************************************************************/
/*                                                                         */
/*                         Electrocardiograma                              */
/*                                                                         */
/***************************************************************************/

/*

    Datos de los electrocardiograma, esta tabla debe tener una entrada
    por cada visita

    id entero clave del registro
    paciente entero clave del paciente
    fecha date fecha de toma del electro
    normal tinyint (0 falso - 1 verdadero)
    bradicardia tinyint (0 falso - 1 verdadero)
    fc (verificar valores posibles)
    arritmia enum
    qrs (verificar valores posibles)
    ejeqrs (verificar valores posibles)
    pr (verificar valores posibles)
    brd tinyint (0 falso - 1 verdadero)
    brdmoderado tinyint (0 falso - 1 verdadero)
    bcrd tinyint (0 falso - 1 verdadero)
    tendenciahbai tinyint (0 falso - 1 verdadero)
    hbai tinyint (0 falso - 1 verdadero)
    tciv tinyint (0 falso - 1 verdadero)
    bcri tinyint (0 falso - 1 verdadero)
    bav1g tinyint (0 falso - 1 verdadero)
    bav2g tinyint (0 falso - 1 verdadero)
    bav3g tinyint (0 falso - 1 verdadero)
    fibrosis tinyint (0 falso - 1 verdadero)
    aumentoai tinyint (0 falso - 1 verdadero)
    wpv tinyint (0 falso - 1 verdadero)
    hvi tinyint (0 falso - 1 verdadero)
    repolarizacion enum
    usuario entero clave del usuario
    fecha_alta date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS electrocardiograma;

-- la recreamos

/***************************************************************************/
/*                                                                         */
/*                           Ecocardiograma                                */
/*                                                                         */
/***************************************************************************/

/*

    Tabla con los datos de los ecocardiogramas, los trae el paciente y pueden
    haber entradas (varias) o ninguna por cada paciente

    id entero clave del registro
    paciente entero clave del paciente
    fecha date fecha de toma
    normal tinyint (0 falso - 1 verdadero)
    ddvi (verificar tipo de dato)
    dsvi (verificar tipo de dato)
    fac (verificar tipo de dato)
    siv (verificar tipo de dato)
    pp (verificar tipo de dato)
    ai (verificar tipo de dato)
    ao (verificar tipo de dato)
    fey (verificar tipo de dato)
    ddvd (verificar tipo de dato)
    ad (verificar tipo de dato)
    movilidad (verificar tipo de dato)
    fsvi (verificar tipo de dato)
    usuario entero clave del usuario
    fecha_alta date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS ecocardiograma;

-- la recreamos

/***************************************************************************/
/*                                                                         */
/*                                 Holter                                  */
/*                                                                         */
/***************************************************************************/

/*

    Esta es la tabla con los datos del holter, puede traerlo el paciente
    se le puede tomar en el instituto o puede no tener

    id clave del registro
    paciente entero clave del paciente
    fecha date fecha de adminstración
    fc (verificar tipo de dato)
    latidos entero número total de latidos
    ev (verificar tipo de dato)
    esv (verificar tipo de dato)
    normal tinyint (0 falso - 1 verdadero)
    bradicardia tinyint (0 falso - 1 verdadero)
    rtacronotropica tinyint (0 falso - 1 verdadero)
    arritmiasevera tinyint (0 falso - 1 verdadero)
    arritmiasimple tinyint (0 falso - 1 verdadero)
    arritmiasupra tinyint (0 falso - 1 verdadero)
    bderama tinyint (0 falso - 1 verdadero)
    bav2g tinyint (0 falso - 1 verdadero)
    disociacion tinyint (0 falso - 1 verdadero)
    comentarios texto comentarios del usuario
    usuario entero clave del usuario
    fecha_alta date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS holter;

-- la recreamos

/***************************************************************************/
/*                                                                         */
/*                            Ergometría                                   */
/*                                                                         */
/***************************************************************************/

/*

    Estos son los datos de la ergometría, los trae el paciente y puede
    haber mas de un registro por cada paciente

    id entero clave del registro
    paciente entero clave del paciente
    fecha date fecha de administración
    fcbasal (verificar tipo de dato)
    fcmax (verificar tipo de dato)
    tabasal (verificar tipo de dato)
    tamax (verificar tipo de dato)
    kpm (verificar tipo de dato)
    itt (verificar tipo de dato)
    observaciones texto comentarios del usuario
    usuario entero clave del usuario
    fecha_alta date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS ergometria;

-- la recreamos


/***************************************************************************/
/*                                                                         */
/*                            Clasificación                                */
/*                                                                         */
/***************************************************************************/

/*

    Esta tabla contiene los datos de la clasificación de la enfermedad de
    Chagas según Kuscknir debería haber una entrada por cada paciente

    id entero clave del registro
    paciente entero clave del paciente
    estadio enum estadio en que se encuentra
    observaciones comentarios del usuario
    usuario entero clave del usuario
    fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS clasificación;

-- la recreamos
CREATE TABLE clasificacion (
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    paciente int(6) UNSIGNED NOT NULL,
    estadio enum("Estadio 0", "Estadio I", "Estadio II", "Estadio III") DEFAULT NULL,
    observaciones mediumtext DEFAULT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY paciente(paciente),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Clasificacion según Kuscknir';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_clasificacion;

-- la recreamos
CREATE TABLE auditoria_clasificacion (
    id int(6) UNSIGNED NOT NULL,
    paciente int(6) UNSIGNED NOT NULL,
    estadio enum("Estadio 0", "Estadio I", "Estadio II", "Estadio III") DEFAULT NULL,
    observaciones mediumtext DEFAULT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY paciente(paciente),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de la clasificación';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_clasificacion;

-- lo recreamos
CREATE TRIGGER edicion_clasificacion
AFTER UPDATE ON clasificacion
FOR EACH ROW
INSERT INTO auditoria_clasificacion
       (id,
        paciente,
        estadio,
        observaciones,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.estadio,
        OLD.observaciones,
        OLD.usuario,
        OLD.fecha,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_clasificacion;

-- lo recreamos
CREATE TRIGGER eliminacion_clasificacion
AFTER DELETE ON clasificacion
FOR EACH ROW
INSERT INTO auditoria_clasificacion
       (id,
        paciente,
        estadio,
        observaciones,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.estadio,
        OLD.observaciones,
        OLD.usuario,
        OLD.fecha,
        "Eliminacion");

-- eliminamos la vista
DROP VIEW IF EXISTS v_clasificacion;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_clasificacion AS
       SELECT diagnostico.clasificacion.id AS id,
              diagnostico.clasificacion.paciente AS paciente,
              diagnostico.clasificacion.estadio AS estadio,
              diagnostico.clasificacion.observaciones AS observaciones,
              diagnostico.clasificacion.usuario AS idusuario,
              cce.responsables.usuario AS usuario,
              DATE_FORMAT(diagnostico.clasificacion.fecha, '%d/%m/%Y') AS fecha
       FROM diagnostico.clasificacion INNER JOIN cce.responsables ON diagnostico.clasificacion.usuario = cce.responsables.id;
