/*

    Nombre: Pedidos
    Fecha: 23/01/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que controla las operaciones sobre la
                 tabla de pedidos

 */

// definición del paquete
package stock;

//inclusión de librerías
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.Image;
import java.sql.Connection;
import java.sql.PreparedStatement;
import dbApi.Conexion;
import seguridad.Seguridad;
import java.sql.Blob;

// definición de la clase
public class Pedidos {

	// definición de variables de clase
	private int Id;             // clave del pedido
	private int IdItem;         // clave del item
	private String Item;        // nombre del item
	private int IdUsuario;      // usuario actual
	private int IdSolicito;     // clave del usuario que realizó el pedido
	private String Usuario;     // nombre del usuario en la base
	private String FechaAlta;   // fecha de alta del registro
	private int Cantidad;       // cantidad solicitada
	private int Existentes;     // existencia en depósito
	private String CodigoSop;   // codigo sop del item
	private int IdLaboratorio;  // clave del laboratorio del usuario
    protected Image Foto;       // imagen almacenada en la base
	private Conexion Enlace;    // puntero a la base

	// constructor de la clase
	public Pedidos() {

		// inicializamos las variables
		this.Id = 0;
		this.IdItem = 0;
		this.Item = "";
		this.Cantidad = 0;
		this.IdUsuario = Seguridad.Id;
		this.IdSolicito = 0;
		this.Usuario = "";
		this.FechaAlta = "";

		this.Foto = null;
		this.IdLaboratorio = Seguridad.Laboratorio;

		// instanciamos la conexión con la base
        this.Enlace = new Conexion();

	}

	// metodos de asignacion de valores
	public void setId(int id) {
		this.Id = id;
	}
	public void setIdItem(int iditem) {
		this.IdItem = iditem;
	}
	public void setCantidad(int cantidad) {
		this.Cantidad = cantidad;
	}

	// metodos de retorno de valores
	public int getId() {
		return this.Id;
	}
	public String getItem() {
		return this.Item;
	}
	public int getIdItem() {
		return this.IdItem;
	}
	public int getCantidad() {
		return this.Cantidad;
	}
	public int getExistentes() {
		return this.Existentes;
	}
	public String getUsuario() {
		return this.Usuario;
	}
	public int getIdSolicito() {
		return this.IdSolicito;
	}
	public String getFechaAlta() {
		return this.FechaAlta;
	}
	public Image getFoto() {
		return this.Foto;
	}
	public String getCodigoSop() {
		return this.CodigoSop;
	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @return vector con los registros
	 * Método que retorna un vector con los pedidos pendientes
	 * del laboratorio del usuario actual
	 */
	public ResultSet pedidosPendientes() {

        // declaración de variables
        String Consulta;
        ResultSet Pendientes;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_pedidos.id_pedido AS id, " +
                   "       diagnostico.v_pedidos.descripcion AS descripcion, " +
        		   "       diagnostico.v_pedidos.codigosop AS codigosop, " +
                   "       diagnostico.v_pedidos.usuario AS usuario, " +
        		   "       diagnostico.v_pedidos.cantidad AS cantidad, " +
                   "       diagnostico.v_pedidos.existencia AS existencia, " +
        		   "       diagnostico.v_pedidos.fecha_alta AS fecha " +
                   "FROM diagnostico.v_pedidos " +
        		   "WHERE diagnostico.v_pedidos.id_laboratorio = '" + this.IdLaboratorio + "' " +
                   "ORDER BY diagnostico.v_pedidos.descripcion; ";

        // ejecutamos la consulta
        Pendientes = this.Enlace.Consultar(Consulta);

        // retornamos el vector
        return Pendientes;

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@ŋmail.com>
	 * @param idusuario - clave del usuario
	 * @return Resultset
	 * Metodo que retorna el vector con todos los pedidos de
	 * un usuario
	 */
	public ResultSet pedidosPendientes(int idusuario) {

        // declaración de variables
        String Consulta;
        ResultSet Pendientes;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_pedidos.id_pedido AS id, " +
                   "       diagnostico.v_pedidos.descripcion AS descripcion, " +
        		   "       diagnostico.v_pedidos.codigosop AS codigosop, " +
                   "       diagnostico.v_pedidos.usuario AS usuario, " +
        		   "       diagnostico.v_pedidos.cantidad AS cantidad, " +
                   "       diagnostico.v_pedidos.existencia AS existencia, " +
        		   "       diagnostico.v_pedidos.fecha_alta AS fecha " +
                   "FROM diagnostico.v_pedidos " +
        		   "WHERE diagnostico.v_pedidos.id_usuario = '" + idusuario + "' " +
                   "ORDER BY diagnostico.v_pedidos.descripcion; ";

        // ejecutamos la consulta
        Pendientes = this.Enlace.Consultar(Consulta);

        // retornamos el vector
        return Pendientes;

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @return int - clave del registro afectado
	 * Metodo que ejecuta la consulta de edicion o insercion en
	 * la base de pedidos segun corresponda
	 */
	public int grabaPedido() {

		// si esta insertando
		if (this.Id == 0) {
			this.nuevoPedido();
		} else {
			this.editaPedido();
		}

		// retornamos la clave
		return this.Id;

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Metodo que ejecuta la consulta de insercion de un
	 * nuevo pedido en la base
	 */
	public void nuevoPedido() {

    	// declaración de variables
    	String Consulta;
    	PreparedStatement preparedStmt;
    	Connection Puntero = this.Enlace.getConexion();

    	// componemos la consulta
    	Consulta = "INSERT INTO diagnostico.pedidos "
    			+ "        (id_item, "
    			+ "         id_usuario, "
    			+ "         cantidad) "
    			+ "        VALUES "
    			+ "        (?,?,?);";

    	try {

    		// asignamos el puntero a la consulta
			preparedStmt = Puntero.prepareStatement(Consulta);

	    	// asignamos los valores
	    	preparedStmt.setInt (1, this.IdItem);
	        preparedStmt.setInt (2, this.IdUsuario);
	        preparedStmt.setInt (3, this.Cantidad);

	        // ejecutamos la consulta
	        preparedStmt.execute();

	        // obtenemos la id
	        this.Id = this.Enlace.UltimoInsertado();

	    // si hubo un error
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Metodo que ejecuta la consulta de edicion de un pedido
	 * en la base
	 */
	public void editaPedido() {

    	// declaración de variables
    	String Consulta;
    	PreparedStatement preparedStmt;
    	Connection Puntero = this.Enlace.getConexion();

    	// componemos la consulta
    	Consulta = "UPDATE diagnostico.pedidos SET"
    			+ "        id_item = ?, "
    			+ "        id_usuario = ?, "
    			+ "        cantidad = ? "
    			+ " WHERE diagnostico.pedidos.id = ?";

    	try {

    		// asignamos el puntero a la consulta
			preparedStmt = Puntero.prepareStatement(Consulta);

	    	// asignamos los valores
	    	preparedStmt.setInt (1, this.IdItem);
	        preparedStmt.setInt (2, this.IdUsuario);
	        preparedStmt.setInt (3, this.Cantidad);
	        preparedStmt.setInt (4, this.Id);

	        // ejecutamos la consulta
	        preparedStmt.execute();

	    // si hubo un error
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param iditem clave del item solicitado
	 * @return boolean
	 * Metodo que verifica que en un alta el pedido no
	 * se encuentre repetido para el mismo usuario
	 */
	public boolean validaPedido(int iditem) {

		// declaracion de variables
        String Consulta;
        boolean Correcto = false;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT COUNT(diagnostico.v_pedidos.id_pedido) AS registros " +
                   "FROM diagnostico.v_pedidos " +
        		   "WHERE diagnostico.v_pedidos.id_item = '" + iditem + "' AND " +
                   "      diagnostico.v_pedidos.id_usuario = '" + this.IdUsuario + "'; ";

        // ejecutamos la consulta y obtenemos el registro
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            Resultado.next();

            // asignamos los valores
            if (Resultado.getInt("registros") == 0) {
            	Correcto = true;
            } else {
            	Correcto = false;
            }

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

        // retornamos
        return Correcto;

	}

	/**
	 * @autor Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param idpedido - entero con la clave de un pedido
	 * Metodo que recibe como parametro la clave de un
	 * pedido y asigna en las variables de clase los
	 * valores del registro
	 */
	public void getDatosPedido(int idpedido) {

		// declaracion de variables
		String Consulta;
		ResultSet Resultado;
		Blob Archivo;

		// componemos la consulta
		Consulta = "SELECT diagnostico.v_pedidos.id_pedido AS id, " +
		           "       diagnostico.v_pedidos.id_item AS iditem, " +
				   "       diagnostico.v_pedidos.descripcion AS item, " +
		           "       diagnostico.v_pedidos.codigosop AS codigosop, " +
				   "       diagnostico.v_pedidos.imagen AS imagen, " +
		           "       diagnostico.v_pedidos.usuario AS usuario, " +
				   "       diagnostico.v_pedidos.id_usuario AS id_solicito, " +
				   "       diagnostico.v_pedidos.fecha_alta AS fecha_alta, " +
				   "       diagnostico.v_pedidos.cantidad AS cantidad, " +
				   "       diagnostico.v_pedidos.existencia AS existentes " +
		           "FROM diagnostico.v_pedidos " +
				   "WHERE diagnostico.v_pedidos.id_pedido = '" + idpedido + "'; ";

		// ejecutamos la consulta
		Resultado = this.Enlace.Consultar(Consulta);

        try {

            // nos movemos al primer registro
            Resultado.next();

            // asignamos a los valores de clase
            this.Id = Resultado.getInt("id");
            this.IdItem = Resultado.getInt("iditem");
            this.Item = Resultado.getString("item");
            this.CodigoSop = Resultado.getString("codigosop");
            this.Cantidad = Resultado.getInt("cantidad");
            this.Existentes = Resultado.getInt("existentes");
			this.FechaAlta = Resultado.getString("fecha_alta");
			this.IdSolicito = Resultado.getInt("id_solicito");
            this.Usuario = Resultado.getString("usuario");

            // ahora leemos el campo blob imagen y lo convertimos
            // verificando que no sea nulo
            if (Resultado.getBlob("imagen") != null){
                Archivo = Resultado.getBlob("imagen");
                this.Foto = javax.imageio.ImageIO.read(Archivo.getBinaryStream());
            }

        // si no hay registros o hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param idpedido - entero con la clave del pedido
	 * Metodo que recibe como parametro la clave de un pedido
	 * y ejecuta la consulta de eliminacion
	 */
	public void borraPedido(int idpedido) {

		// declaracion de variables
		String Consulta;

		// componemos la consulta y la ejecutamos directamente
		Consulta = "DELETE FROM diagnostico.pedidos WHERE id = '" + idpedido + "';";
		this.Enlace.Ejecutar(Consulta);

	}

}
