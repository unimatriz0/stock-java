/*

    Nombre: FormInicio
    Fecha: 24/01/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: stock
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que presenta el formulario con los botones
                 de acción del inventario

 */

package stock;

// importamos las librerías
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JOptionPane;
import financiamiento.FormFinanciamiento;
import usuarios.FormUsuarios;
import laboratorios.FormLaboratorios;
import seguridad.Seguridad;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Font;
import seguridad.formNuevoPass;
import dbApi.FormConfig;

// definición de la clase
public class FormInicio extends JFrame {

	// definimos el serial id del formulario
	private static final long serialVersionUID = -8046739924923800912L;

	// definición de variables de clase
	private JPanel contentPane;

	// constructor de la clase
	public FormInicio() {

		// fijamos la operación del botón cerrar
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// las propiedades del formulario
        this.setTitle("Control de Stock");
        this.setIconImage(new ImageIcon(getClass().getResource("/Graficos/logo_fatala.png")).getImage());
		this.setBounds(100, 100, 534, 340);
		this.contentPane = new JPanel();
		this.setContentPane(this.contentPane);
		this.contentPane.setLayout(null);

		// llamamos la rutina de inicialización
		this.initForm();

	}

	// método que inicializa los componentes del formulario
	protected void initForm() {

		// presenta el título
		JLabel lTitulo = new JLabel("Sistema de Control de Stock");
		lTitulo.setFont(new Font("Dialog", Font.BOLD, 14));
		lTitulo.setBounds(171, 10, 231, 15);
		this.contentPane.add(lTitulo);

		// presenta el encabezado
		JLabel lEncabezado = new JLabel("Pulse sobre la acción que desea realizar");
		lEncabezado.setBounds(34, 45, 439, 28);
		this.contentPane.add(lEncabezado);

		// presenta el botón cambiar contraseña
		JButton btnPassword = new JButton("Contraseña");
		btnPassword.setToolTipText("Cambiar la contraseña de ingreso");
		btnPassword.setBounds(30, 90, 130, 30);
		btnPassword.setIcon(new ImageIcon(getClass().getResource("/Graficos/m39.png")));
        btnPassword.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                password();
            }
        });
		this.contentPane.add(btnPassword);

		// presenta el botón de configuración
		JButton btnConfigurar = new JButton("Configurar");
		btnConfigurar.setToolTipText("Configurar la apariencia");
		btnConfigurar.setBounds(200, 90, 130, 30);
		btnConfigurar.setIcon(new ImageIcon(getClass().getResource("/Graficos/majustes.png")));
        btnConfigurar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                configurar();
            }
        });
		this.contentPane.add(btnConfigurar);

		// el botón del diccionario de artículos
		JButton btnArticulos = new JButton("Artículos");
		btnArticulos.setToolTipText("Altas y Bajas del diccionario de elementos");
		btnArticulos.setBounds(365, 90, 130, 30);
		btnArticulos.setIcon(new ImageIcon(getClass().getResource("/Graficos/mquimica.png")));
        btnArticulos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                articulos();
            }
        });
		this.contentPane.add(btnArticulos);

		// el botón de pedidos
		JButton btnPedidos = new JButton("Pedidos");
		btnPedidos.setToolTipText("Realizar pedidos de mercadería o autorizarlos");
		btnPedidos.setBounds(30, 145, 130, 30);
		btnPedidos.setIcon(new ImageIcon(getClass().getResource("/Graficos/mmarcador.png")));
        btnPedidos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pedidos();
            }
        });
		this.contentPane.add(btnPedidos);

		// el botón de inventario
		JButton btnInventario = new JButton("Inventario");
		btnInventario.setToolTipText("Ver existencias en depósito");
		btnInventario.setBounds(200, 145, 130, 30);
		btnInventario.setIcon(new ImageIcon(getClass().getResource("/Graficos/minventario.png")));
        btnInventario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                inventario();
            }
        });
		this.contentPane.add(btnInventario);

		// si el usuario está autorizado
		if (Seguridad.Stock.equals("Si")) {

			// agrega el botón de usuarios
			JButton btnUsuarios = new JButton("Usuarios");
			btnUsuarios.setToolTipText("Autorizar usuarios en el sistema");
			btnUsuarios.setBounds(365, 145, 130, 30);
			btnUsuarios.setIcon(new ImageIcon(getClass().getResource("/Graficos/musuarios.png")));
	        btnUsuarios.addActionListener(new java.awt.event.ActionListener() {
	            public void actionPerformed(java.awt.event.ActionEvent evt) {
	                usuarios();
	            }
	        });
			this.contentPane.add(btnUsuarios);

			// agrega el botón ingresar mercadería
			JButton btnIngresos = new JButton("Ingresos");
			btnIngresos.setToolTipText("Ingresar mercadería al depósito");
			btnIngresos.setBounds(30, 200, 130, 30);
			btnIngresos.setIcon(new ImageIcon(getClass().getResource("/Graficos/mchecklist.png")));
	        btnIngresos.addActionListener(new java.awt.event.ActionListener() {
	            public void actionPerformed(java.awt.event.ActionEvent evt) {
	                ingresos();
	            }
	        });
			this.contentPane.add(btnIngresos);

			// el botón entregar mercadería
			JButton btnEgresos = new JButton("Egresos");
			btnEgresos.setToolTipText("Entregar mercadería");
			btnEgresos.setBounds(200, 200, 130, 30);
			btnEgresos.setIcon(new ImageIcon(getClass().getResource("/Graficos/msalida.png")));
	        btnEgresos.addActionListener(new java.awt.event.ActionListener() {
	            public void actionPerformed(java.awt.event.ActionEvent evt) {
	                egresos();
	            }
	        });
			this.contentPane.add(btnEgresos);

			// el botón financiamiento
			JButton btnFinanciamiento = new JButton("Financiamiento");
			btnFinanciamiento.setToolTipText("Diccionario de fuentes de financiamiento");
			btnFinanciamiento.setBounds(365, 200, 130, 30);
			btnFinanciamiento.setIcon(new ImageIcon(getClass().getResource("/Graficos/mdolar.jpeg")));
	        btnFinanciamiento.addActionListener(new java.awt.event.ActionListener() {
	            public void actionPerformed(java.awt.event.ActionEvent evt) {
	                financiamiento();
	            }
	        });
			this.contentPane.add(btnFinanciamiento);

			// agregamos el botón laboratorios
			JButton btnLaboratorios = new JButton("Laboratorios");
			btnLaboratorios.setToolTipText("Laboratorios registrados en el sistema");
			btnLaboratorios.setBounds(30, 255, 130, 30);
			btnLaboratorios.setIcon(new ImageIcon(getClass().getResource("/Graficos/mHospital.png")));
	        btnLaboratorios.addActionListener(new java.awt.event.ActionListener() {
	            public void actionPerformed(java.awt.event.ActionEvent evt) {
	                laboratorios();
	            }
	        });
			this.contentPane.add(btnLaboratorios);

			// agrega el botón de reportes
			JButton btnReportes = new JButton("Reportes");
			btnReportes.setToolTipText("Reportes del Sistema");
			btnReportes.setBounds(200, 255, 130, 30);
			btnReportes.setIcon(new ImageIcon(getClass().getResource("/Graficos/mbase-de-datos.png")));
	        btnReportes.addActionListener(new java.awt.event.ActionListener() {
	            public void actionPerformed(java.awt.event.ActionEvent evt) {
	                reportes();
	            }
	        });
			this.contentPane.add(btnReportes);

		}

        // agrega el botón salir
        JButton btnSalir = new JButton("Salir");
        btnSalir.setToolTipText("Abandona la aplicación");
        btnSalir.setBounds(365, 255, 130, 30);
        btnSalir.setIcon(new ImageIcon(getClass().getResource("/Graficos/msalida.png")));
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Salir();
            }
        });
        this.contentPane.add(btnSalir);

        // verifica el stock crítico
        this.verificaCritico();

	}

	// método llamado al pulsar el botón password
	protected void password(){

		// instanciamos el formulario
		formNuevoPass nuevoPassword = new formNuevoPass(this, true);
		nuevoPassword.setVisible(true);

	}

	// método llamado al pulsar el botón ajustes
	protected void configurar(){

		// instanciamos y mostramos el formulario
		FormConfig Configurar = new FormConfig(this, true);
		Configurar.setVisible(true);

	}

	// método llamado al pulsar el botón artículos
	protected void articulos() {
        FormGrillaItems Items = new FormGrillaItems(this, true);
        Items.setVisible(true);
	}

	// método llamado al pulsar el botón pedidos
	protected void pedidos() {
		FormPedidos Pedidos = new FormPedidos(this, true);
		Pedidos.setVisible(true);
	}

	// método llamado al pulsar el botón inventario
	protected void inventario() {
		FormGrillaInventario Inventario = new FormGrillaInventario(this, true);
		Inventario.setVisible(true);
	}

	// método llamado al pulsar el botón usuarios
	protected void usuarios(){
		FormUsuarios Usuarios = new FormUsuarios(this, true);
		Usuarios.setVisible(true);
	}

	// método llamado al pulsar el botón ingresos
	protected void ingresos() {
		FormGrillaIngresos Ingresos = new FormGrillaIngresos(this, true);
		Ingresos.setVisible(true);
	}

	// método llamado al pulsar el botón egresos
	protected void egresos() {
		FormGrillaEgresos Egresos = new FormGrillaEgresos(this, true);
		Egresos.setVisible(true);
	}

	// método llamado al pulsar el botón financiamiento
	protected void financiamiento() {
		FormFinanciamiento Fuentes = new FormFinanciamiento(this, true);
		Fuentes.setVisible(true);
	}

	// método llamado al pulsar sobre el botón laboratorios
	protected void laboratorios(){
		FormLaboratorios Laboratorio = new FormLaboratorios(this, true);
		Laboratorio.setVisible(true);
	}

	// método llamado al pulsar el botón reportes
	protected void reportes(){

		// instanciamos el formulario y lo mostramos
		FormReportes Informes = new FormReportes(this, true);
		Informes.setVisible(true);

	}

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al iniciar el formulario que verifica
     * si existe stock crítico y presenta el alerta
     */
    protected void verificaCritico(){

        // si el usuario está autorizado
        if (Seguridad.Stock.equals("Si")){

			// instanciamos la clase y obtenemos el crítico
			Stock Deposito = new Stock();
			int Critico = Deposito.verificaCritico();

			// si hay elementos
			if (Critico != 0){

				// presenta el mensaje
				String mensaje = "Existen artículos por debajo del\n";
				mensaje += "valor crítico. Verifique por favor";
				JOptionPane.showMessageDialog(this,
											  mensaje,
											  "Atención", JOptionPane.INFORMATION_MESSAGE);

			}

        }

    }

    // método llamado al pulsar el botón salir
    protected void Salir(){

        // abandona el formulario
        this.dispose();

    }

}
