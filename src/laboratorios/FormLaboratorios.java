/*

    Nombre: FormLaboratorios
    Fecha: 28/02/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Diagnóstico
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Método que arma el formulario del abm de laboratorios

 */

 // definición del paquete
package laboratorios;

// importamos las librerìas
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTextArea;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import dependencias.Dependencias;
import funciones.ComboClave;
import funciones.Imagenes;
import funciones.Utilidades;
import javax.swing.JPopupMenu.Separator;
import seguridad.Seguridad;
import localidades.Localidades;
import paises.Paises;
import mapas.FormMapas;

// definición de la clase
public class FormLaboratorios extends JDialog {

	// añadimos el serial id
	private static final long serialVersionUID = -6526841964705075217L;

	// definimos las variables
	private JTextField tId;                         // clave del registro
	private JTextField tNombre;                     // nombre del laboratorio
	private JTextField tCoordenadas;                // coordenadas gps
	private JTextField tResponsable;                // nombre del responsable
	private JTextField tCodigoPostal;               // código postal
	private JTextField tDomicilio;                  // domicilio postal
	public JTextField tPais;                        // nombre del país
	public JTextField tJurisdiccion;                // nombre de la provincia
	public JTextField tLocalidad;                   // nombre de la localidad
	public JTextField tEmail;                       // correo electrónico
	private JComboBox<Object> cDependencia;         // dependencia administrativa
	private JTextArea tComentarios;                 // comentarios y observaciones
	private JLabel lLogo;                           // logo del laboratorio
	private JTextField tUsuario;                    // usuario que ingresó el registro
	private JTextField tAlta;                       // fecha de alta del registro

	// variables de clase
	public String CodLoc;                       // clave indec de la localidad
	public int IdPais;                          // clave del país
    private String Archivo;                     // puntero del archivo de imagen
    private FileInputStream Contenido;          // contenido del archivo
	private int Longitud;                       // tamaño del archivo
	private Localidades Ciudades;               // clase de localidades
	private Laboratorios Instituciones;         // clase de laboratorios
	public boolean Modificado;                  // switch de registro modificado

	// constructor de la clase
	public FormLaboratorios(java.awt.Frame parent, boolean modal) {

        // setea el padre e inicia los componentes
        super(parent, modal);

		// fijamos las propiedades del formulario
		setBounds(100, 100, 730, 420);
		getContentPane().setLayout(null);
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.setTitle("Laboratorios Registrados");

		// inicializamos la interfaz
		this.initForm();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que inicializa el formulario
	 */
	protected void initForm(){

		// inicializamos las variables
		this.CodLoc = "";
		this.IdPais = 0;
		this.Archivo = "";
		this.Contenido = null;
		this.Longitud = 0;
		this.Modificado = false;

		// instanciamos la clase de localidades
		this.Ciudades = new Localidades();
		this.Instituciones = new Laboratorios();

		// define la barra de menu
        JMenuBar barraMenu = new JMenuBar();

        // define el menu de laboratorios
        JMenu mLaboratorios = new JMenu("Laboratorios");
        mLaboratorios.setIcon(new ImageIcon(getClass().getResource("/Graficos/mHospital.png")));

        // si es administrador
        if (Seguridad.Administrador.equals("Si")){

            // el elemento nuevo laboratorio
            JMenuItem mNuevoLaboratorio = new JMenuItem("Nuevo Laboratorio");
            mNuevoLaboratorio.setIcon(new ImageIcon(getClass().getResource("/Graficos/manadir.png")));
            mNuevoLaboratorio.setToolTipText("Ingresa un nuevo laboratorio en la base");
            mNuevoLaboratorio.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    cancelaFormulario();
                }

            });
            mLaboratorios.add(mNuevoLaboratorio);

        }

        // el elemento buscar laboratorio
        JMenuItem mBuscaLaboratorio = new JMenuItem("Buscar");
        mBuscaLaboratorio.setIcon(new ImageIcon(getClass().getResource("/Graficos/mbinoculares.png")));
        mBuscaLaboratorio.setToolTipText("Busca un laboratorio en la base");
        mBuscaLaboratorio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buscaLaboratorio();
            }
        });
        mLaboratorios.add(mBuscaLaboratorio);

        // el separador
        Separator jSeparator = new JPopupMenu.Separator();
        mLaboratorios.add(jSeparator);

        // el menú salir
        JMenuItem mSalir = new JMenuItem("Salir");
        mSalir.setIcon(new ImageIcon(getClass().getResource("/Graficos/msalida.png")));
        mSalir.setToolTipText("Cerrar este formulario");
        mSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Salir();
            }
        });
        mLaboratorios.add(mSalir);

        // agregamos el menú a la barra
        barraMenu.add(mLaboratorios);

        // agregamos la barra a la interfaz
        this.setJMenuBar(barraMenu);

		// el tìtulo del laboratorio
		JLabel lTitulo = new JLabel("Laboratorios Registrados en el Sistema");
		lTitulo.setBounds(10, 10, 585, 26);
		getContentPane().add(lTitulo);

		// la clave del registro
		JLabel lId = new JLabel("ID:");
		lId.setBounds(10, 45, 29, 26);
		getContentPane().add(lId);
		this.tId = new JTextField();
		this.tId.setBounds(30, 45, 45, 26);
		this.tId.setToolTipText("Clave del registro");
		this.tId.setEditable(false);
		getContentPane().add(this.tId);

		// el nombre del laboratorio
		JLabel lNombre = new JLabel("Nombre:");
		lNombre.setBounds(90, 45, 70, 26);
		getContentPane().add(lNombre);
		this.tNombre = new JTextField();
		this.tNombre.setBounds(145, 45, 500, 26);
		this.tNombre.setToolTipText("Nombre completo del laboratorio");
        this.tNombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tNombrePulsado(evt);
            }
        });
		getContentPane().add(this.tNombre);

		// el botón mapa
		JButton btnMapa = new JButton();
		btnMapa.setBounds(650, 45, 26, 26);
		btnMapa.setToolTipText("Pulse para ver el mapa");
		btnMapa.setIcon(new ImageIcon(getClass().getResource("/Graficos/mmarcador-de-posicion.png")));
        btnMapa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                verMapa();
            }
        });
		getContentPane().add(btnMapa);

		// las coordenadas gps (ocultas)
		this.tCoordenadas = new JTextField();
		this.tCoordenadas.setBounds(581, 45, 114, 19);
		this.tCoordenadas.setVisible(false);
		getContentPane().add(this.tCoordenadas);

		// el responsable
		JLabel lblResponsable = new JLabel("Responsable:");
		lblResponsable.setBounds(10, 80, 108, 26);
		getContentPane().add(lblResponsable);
		this.tResponsable = new JTextField();
		this.tResponsable.setBounds(105, 80, 500, 26);
		this.tResponsable.setToolTipText("Nombre del responsable del laboratorio");
        this.tResponsable.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tResponsablePulsado(evt);
            }
        });
		getContentPane().add(this.tResponsable);

		// el domicilio
		JLabel lDomicilio = new JLabel("Domicilio:");
		lDomicilio.setBounds(10, 115, 70, 26);
		getContentPane().add(lDomicilio);
		this.tDomicilio = new JTextField();
		this.tDomicilio.setBounds(75, 115, 383, 26);
		this.tDomicilio.setToolTipText("Domicilio postal del laboratorio");
        this.tDomicilio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tDomicilioPulsado(evt);
            }
        });
		getContentPane().add(this.tDomicilio);

		// el código postal
		JLabel lCodigoPostal = new JLabel("Código Postal:");
		lCodigoPostal.setBounds(470, 115, 103, 26);
		getContentPane().add(lCodigoPostal);
		this.tCodigoPostal = new JTextField();
		this.tCodigoPostal.setBounds(565, 115, 80, 26);
		this.tCodigoPostal.setToolTipText("Código Postal del domicilio");
        this.tCodigoPostal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tCodigoPulsado(evt);
            }
        });
		getContentPane().add(this.tCodigoPostal);

		// el país (solo lectura)
		JLabel lPais = new JLabel("País:");
		lPais.setBounds(10, 150, 45, 26);
		getContentPane().add(lPais);
		this.tPais = new JTextField();
		this.tPais.setBounds(45, 150, 128, 26);
		this.tPais.setEditable(false);
		this.tPais.setToolTipText("País donde funciona el laboratorio");
		getContentPane().add(this.tPais);

		// la jurisdicción (solo lectura)
		JLabel lJurisdiccion = new JLabel("Jurisdicción:");
		lJurisdiccion.setBounds(188, 150, 97, 26);
		getContentPane().add(lJurisdiccion);
		this.tJurisdiccion= new JTextField();
		this.tJurisdiccion.setBounds(270, 150, 145, 26);
		this.tJurisdiccion.setEditable(false);
		this.tJurisdiccion.setToolTipText("Jurisdicción del laboratorio");
		getContentPane().add(this.tJurisdiccion);

		// la localidad
		JLabel lLocalidad = new JLabel("Localidad:");
		lLocalidad.setBounds(433, 150, 85, 26);
		getContentPane().add(lLocalidad);
		this.tLocalidad = new JTextField();
		this.tLocalidad.setBounds(500, 150, 180, 26);
		this.tLocalidad.setToolTipText("Ingrese parte de la localidad y pulse buscar");
		getContentPane().add(this.tLocalidad);

		// el botón buscar localidad
		JButton btnBuscaLocalidad = new JButton("");
		btnBuscaLocalidad.setBounds(690, 150, 26, 26);
		btnBuscaLocalidad.setToolTipText("Pulse para buscar la localdidad");
		btnBuscaLocalidad.setIcon(new ImageIcon(getClass().getResource("/Graficos/mbusqueda.png")));
        btnBuscaLocalidad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buscaLocalidad();
            }
        });
		getContentPane().add(btnBuscaLocalidad);

		// el mail
		JLabel lEmail = new JLabel("E-Mail:");
		lEmail.setBounds(10, 185, 56, 26);
		getContentPane().add(lEmail);
		this.tEmail = new JTextField();
		this.tEmail.setBounds(58, 185, 227, 26);
		this.tEmail.setToolTipText("Dirección de Correo Electrónico");
        this.tEmail.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tEmailPulsado(evt);
            }
        });
		getContentPane().add(this.tEmail);

		// la dependencia
		JLabel lDependencia = new JLabel("Dependencia:");
		lDependencia.setBounds(303, 185, 108, 26);
		getContentPane().add(lDependencia);
		this.cDependencia = new JComboBox<>();
		this.cDependencia.setBounds(400, 185, 163, 26);
		this.cDependencia.setToolTipText("Dependencia de la institución");
		this.cDependencia.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                registroModificado();
            }
        });
		getContentPane().add(this.cDependencia);

		// los comentarios
		this.tComentarios= new JTextArea();
		this.tComentarios.setBounds(10, 220, 352, 110);
		this.tComentarios.setToolTipText("Ingrese sus comentarios y observaciones");
		getContentPane().add(this.tComentarios);

		// el logo del laboratorio
		this.lLogo = new JLabel("");
		this.lLogo.setForeground(Color.BLACK);
		this.lLogo.setBounds(375, 220, 130, 130);
        this.lLogo.setIcon(new ImageIcon(getClass().getResource("/Graficos/sin_imagen.jpg")));
		this.lLogo.setToolTipText("Pulse para cargar el logo del laboratorio");
		getContentPane().add(this.lLogo);
        this.lLogo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lLogoPulsado(evt);
            }
        });

		// el usuario
		JLabel lUsuario = new JLabel("Usuario:");
		lUsuario.setBounds(537, 220, 70, 26);
		getContentPane().add(lUsuario);
		this.tUsuario = new JTextField();
		this.tUsuario.setBounds(601, 220, 97, 26);
		this.tUsuario.setToolTipText("Usuario que ingresó el registro");
		this.tUsuario.setEditable(false);
		getContentPane().add(this.tUsuario);

		// la fecha de alta
		JLabel lAlta = new JLabel("Alta:");
		lAlta.setBounds(543, 255, 45, 26);
		getContentPane().add(lAlta);
		this.tAlta = new JTextField();
		this.tAlta.setBounds(601, 255, 94, 26);
		this.tAlta.setToolTipText("Fecha de alta del registro");
		this.tAlta.setEditable(false);
		getContentPane().add(this.tAlta);

		// el botón grabar
		JButton btnGrabar = new JButton("Grabar");
		btnGrabar.setBounds(544, 290, 110, 26);
		btnGrabar.setToolTipText("Pulse para grabar el registro");
		btnGrabar.setIcon(new ImageIcon(getClass().getResource("/Graficos/mgrabar.png")));
        btnGrabar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                validaFormulario();
            }
        });
		getContentPane().add(btnGrabar);

		// el botón cancelar
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(544, 325, 110, 26);
		btnCancelar.setToolTipText("Pulse para reiniciar el formulario");
		btnCancelar.setIcon(new ImageIcon(getClass().getResource("/Graficos/mBorrar.png")));
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelaFormulario();
            }
        });
		getContentPane().add(btnCancelar);

		// carga el combo de dependencias
		this.cargaDependencias();

		// fijamos el usuario y la fecha alta
		this.tUsuario.setText(Seguridad.Usuario);
		Utilidades Herramientas = new Utilidades();
		this.tAlta.setText(Herramientas.FechaActual());

		// inicializa el switch de modificado
		this.sinModificar();

		// setea el foco en el nombre
		this.tNombre.requestFocus();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado en el inicio que carga el combo de
	 * dependencias
	 */
	protected void cargaDependencias(){

    	// declaración de variables
    	ResultSet Nomina;

    	// instanciamos la clase
    	Dependencias Fuentes = new Dependencias();

    	// obtenemos la nómina
    	Nomina = Fuentes.nominaDependencias();

        try {

			// agregamos el primer elemento usamos la clase comboclave
			// para almacenar tanto la id como el texto
			this.cDependencia.addItem(new ComboClave(0,""));

			// verificamos si está vacío
			if (!Nomina.next()){

				// presenta el mensaje y cierra el formulario
				JOptionPane.showMessageDialog(this, "Debe cargar las dependencias", "Error", JOptionPane.ERROR_MESSAGE);
				return;

			}

            // nos desplazamos al inicio del resultset
            Nomina.beforeFirst();

            // iniciamos un bucle recorriendo el vector
            while (Nomina.next()){

                // agregamos el registro
				this.cDependencia.addItem(new ComboClave(Nomina.getInt("id"), Nomina.getString("dependencia")));

            }

        // si hubo un error
        } catch (SQLException ex){

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

	}

    // evento al pulsar una tecla sobre el nombre del laboratorio
    private void tNombrePulsado(java.awt.event.KeyEvent evt) {

        // si pulsó enter
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            this.tResponsable.requestFocus();
        } else {
			this.registroModificado();
		}

    }

    // evento al pulsar una tecla sobre el domicilio
    private void tResponsablePulsado(java.awt.event.KeyEvent evt) {

        // si pulsó enter
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            this.tDomicilio.requestFocus();
        } else {
			this.registroModificado();
		}

    }

    // evento al pulsar una tecla sobre el nombre del laboratorio
    private void tDomicilioPulsado(java.awt.event.KeyEvent evt) {

        // si pulsó enter
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            this.tCodigoPostal.requestFocus();
        } else {
			this.registroModificado();
		}

    }

    // evento al pulsar una tecla sobre el código postal
    private void tCodigoPulsado(java.awt.event.KeyEvent evt) {

        // si pulsó enter
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            this.tLocalidad.requestFocus();
        } else {
			this.registroModificado();
		}

    }

    // evento al pulsar una tecla sobre el correo electrónico
    private void tEmailPulsado(java.awt.event.KeyEvent evt) {

        // si pulsó enter
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            this.cDependencia.requestFocus();
        } else {
			this.registroModificado();
		}

    }

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que limpia el formulario al pulsar el botón cancelar
	 * o al ser llamado desde el menú nuevo
	 */
	protected void cancelaFormulario(){

		// si el registro está modificado
		if (this.Modificado){

			// pide confirmación
			if (!this.confirmaAbandona()){
				return;
			}

		}

		// limpiamos los componentes y las variables de clase
		this.tId.setText("");
		this.tNombre.setText("");
		this.tCoordenadas.setText("");
		this.tResponsable.setText("");
		this.tDomicilio.setText("");
		this.tCodigoPostal.setText("");
		this.tPais.setText("");
		this.tJurisdiccion.setText("");
		this.tLocalidad.setText("");
		this.CodLoc = "";
		this.tEmail.setText("");
		ComboClave dependencia = new ComboClave(0,"");
		this.cDependencia.setSelectedItem(dependencia);
		this.tComentarios.setText("");
		this.tUsuario.setText(Seguridad.Usuario);
		Utilidades Herramientas = new Utilidades();
		this.tAlta.setText(Herramientas.FechaActual());
		this.lLogo.setIcon(new ImageIcon(getClass().getResource("/Graficos/sin_imagen.jpg")));

		// actualizamos el switch
		this.sinModificar();

		// fijamos el foco
		this.tNombre.requestFocus();

	}

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón grabar que verifica los
     * datos del formulario
     */
    protected void validaFormulario(){

		// si no ingresó el nombre
		if (this.tNombre.getText().isEmpty()){

            // presenta el mensaje
			JOptionPane.showMessageDialog(this,
						"Ingrese el nombre del laboratorio",
						"Error",
						JOptionPane.ERROR_MESSAGE);
			this.tNombre.requestFocus();
			return;

		}

		// si no ingresó el responsable
		if (this.tResponsable.getText().isEmpty()){

            // presenta el mensaje
			JOptionPane.showMessageDialog(this,
						"Ingrese el nombre del responsable\ndel laboratorio",
						"Error",
						JOptionPane.ERROR_MESSAGE);
			this.tResponsable.requestFocus();
			return;

		}

		// si no ingresó la dirección
		if (this.tDomicilio.getText().isEmpty()){

            // presenta el mensaje
			JOptionPane.showMessageDialog(this,
						"Ingrese el domicilio del laboratorio",
						"Error",
						JOptionPane.ERROR_MESSAGE);
			this.tDomicilio.requestFocus();
			return;

		}

		// el código postal lo permite en blanco

		// si no seleccionó correctamente la localidad
		if (this.CodLoc.equals("")){

			// compone el mensaje
			String mensaje = "Indique la localidad del laboratorio\n";
			mensaje += "ingrese parte del nombre y luego pulse\n";
			mensaje += "el botón Buscar";
            // presenta el mensaje
			JOptionPane.showMessageDialog(this,
						mensaje,
						"Error",
						JOptionPane.ERROR_MESSAGE);
			this.tLocalidad.requestFocus();
			return;

		}

		// si no ingresó el mail
		if (this.tEmail.getText().isEmpty()){

            // presenta el mensaje
			JOptionPane.showMessageDialog(this,
						"Ingrese el mail del laboratorio",
						"Error",
						JOptionPane.ERROR_MESSAGE);
			this.tEmail.requestFocus();
			return;

		// si ingresó
		} else {

			// instanciamos las herramientas
			Utilidades Herramientas = new Utilidades();

			// verificamos que el mail sea correcto
			if (!Herramientas.esEmailCorrecto(this.tEmail.getText())){

				// presenta el mensaje
				JOptionPane.showMessageDialog(this,
							"El mail parece incorrecto. Verifique",
							"Error",
							JOptionPane.ERROR_MESSAGE);
				this.tEmail.requestFocus();
				return;

			}

		}

		// si no seleccionó la dependencia
		ComboClave dependencia = (ComboClave) this.cDependencia.getSelectedItem();
		if (dependencia == null){

            // presenta el mensaje
			JOptionPane.showMessageDialog(this,
						"Seleccione la dependencia de la lista",
						"Error",
						JOptionPane.ERROR_MESSAGE);
			this.cDependencia.requestFocus();
			return;

		}

		// si está insertando
		if (this.tId.getText().isEmpty()){

			// verifica que el mail no esté repetido
			if(!this.Instituciones.verificaMail(this.tEmail.getText())){

				// presenta el mensaje
				JOptionPane.showMessageDialog(this,
							"Ese mail ya se encuentra declarado",
							"Error",
							JOptionPane.ERROR_MESSAGE);
				this.tEmail.requestFocus();
				return;

			}

			// verifica que el nombre no esté repetido
			if (!this.Instituciones.verificaLaboratorio(this.tNombre.getText(), this.CodLoc)){

				// presenta el mensaje
				JOptionPane.showMessageDialog(this,
							"Ese laboratorio ya está declarado",
							"Error",
							JOptionPane.ERROR_MESSAGE);
				this.tNombre.requestFocus();
				return;

			}

		}

		// si llegó hasta aquí graba el registro
		this.grabaLaboratorio();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de validar el formulario que ejecuta
     * la consulta de grabación
     */
    protected void grabaLaboratorio(){

		// fijamos las propiedades de la clase

		// si está editando
		if (!this.tId.getText().isEmpty()){
			this.Instituciones.setIdLaboratorio(Integer.parseInt(this.tId.getText()));
		} else {
			this.Instituciones.setIdLaboratorio(0);
		}

		// asignamos el resto de las propiedades
		this.Instituciones.setNombre(this.tNombre.getText());
		this.Instituciones.setResponsable(this.tResponsable.getText());
		this.Instituciones.setDireccion(this.tDomicilio.getText());
		this.Instituciones.setCodigoPostal(this.tCodigoPostal.getText());
		this.Instituciones.setIdLocalidad(this.CodLoc);
		this.Instituciones.setEmail(this.tEmail.getText());
		this.Instituciones.setObservaciones(this.tComentarios.getText());

		// obtenemos la clave de la dependencia
		ComboClave dependencia = (ComboClave) this.cDependencia.getSelectedItem();
		this.Instituciones.setIdDependencia(dependencia.getClave());

		// si existe un logo
        if (this.Archivo != null) {

            // fijamos las propiedades de la imagen
            this.Instituciones.setImagen(this.Contenido);
            this.Instituciones.setLongImagen(this.Longitud);

		// si no cargó
        } else {

			// lo inicializamos
			this.Instituciones.setImagen(null);
			this.Instituciones.setLongImagen(0);

		}

        // obtenemos la clave del país a partir del nombre
        Paises Naciones = new Paises();
        this.IdPais = Naciones.getClavePais(this.tPais.getText());
        		
		// las coordenadas las obtiene la clase antes de grabar el registro
		// pero debemos pasarle el nombre del país, localidad y provincia
		this.Instituciones.setPais(this.tPais.getText());
		this.Instituciones.setIdPais(this.IdPais);
		this.Instituciones.setProvincia(this.tJurisdiccion.getText());
		this.Instituciones.setLocalidad(this.tLocalidad.getText());

		// grabamos el registro
		int id = this.Instituciones.grabarLaboratorio();

		// actualizamos la id y las coordenadas
		this.tId.setText(Integer.toString(id));
		this.tCoordenadas.setText(this.Instituciones.getCoordenadas());

		// actualizamos el switch
		this.sinModificar();

		// fijamos el foco
		this.tNombre.requestFocus();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar sobre el botón buscar de la
     * localidad, que abre el formulario de búsqueda
     */
    protected void buscaLocalidad(){

    	// si no ingresó parte de la localidad
        if (this.tLocalidad.getText().equals("")) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
            		                      "Ingrese parte del nombre de la localidad",
            		                      "Error",
            		                      JOptionPane.ERROR_MESSAGE);
            this.tLocalidad.requestFocus();
            return;

        }

		ResultSet nomina = this.Ciudades.buscaLocalidad(this.tLocalidad.getText());

		try {

			// si hubo registros
			if (nomina.next()){

				// instanciamos el formulario y cargamos el resultado
				FormBuscaLocalidad locEncontradas = new FormBuscaLocalidad(this, true, this);
				locEncontradas.cargaLocalidades(nomina);
				locEncontradas.setVisible(true);

			// si no hubo registros
			} else {

				// presentamos el alerta
				JOptionPane.showMessageDialog(this,
											"No se han encontrado localidades",
											"Error",
											JOptionPane.ERROR_MESSAGE);
				this.tLocalidad.requestFocus();
				return;

			}

        // si hubo un error
		} catch (SQLException ex) {

			// presenta el mensaje
			System.out.println(ex.getMessage());

		}

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - clave del registro
     * Método que recibe como parámetro la clave de un registro
     * y carga los datos del mismo en el formulario
     */
    public void verLaboratorio(int clave){

		// obtenemos los datos del laboratorio
		this.Instituciones.getDatosLaboratorio(clave);

		// asignamos en el formulario
		this.tId.setText(Integer.toString(this.Instituciones.getIdLaboratorio()));
		this.tNombre.setText(this.Instituciones.getNombre());
		this.tResponsable.setText(this.Instituciones.getResponsable());
		this.tDomicilio.setText(this.Instituciones.getDireccion());
		this.tCodigoPostal.setText(this.Instituciones.getCodigoPostal());
		this.tPais.setText(this.Instituciones.getPais());
		this.IdPais = this.Instituciones.getIdPais();
		this.tJurisdiccion.setText(this.Instituciones.getProvincia());
		this.tLocalidad.setText(this.Instituciones.getLocalidad());
		this.CodLoc = this.Instituciones.getIdLocalidad();
		this.tEmail.setText(this.Instituciones.getEmail());
		this.tComentarios.setText(this.Instituciones.getObservaciones());
		this.tCoordenadas.setText(this.Instituciones.getCoordenadas());
		this.tUsuario.setText(this.Instituciones.getUsuario());
		this.tAlta.setText(this.Instituciones.getFechaAlta());

		// fijamos el combo de dependencia
		ComboClave dependencia = new ComboClave(this.Instituciones.getIdDependencia(),this.Instituciones.getDependencia());
		this.cDependencia.setSelectedItem(dependencia);

		// si tiene imagen
		if (this.Instituciones.getLogo() != null){

            // leemos de la base de datos
            ImageIcon Foto = new ImageIcon(this.Instituciones.getLogo());

            // la escalamos
            Imagenes foto = new Imagenes();
            ImageIcon ajustada = foto.redimensionar(Foto, 130, 130);

            // la asignamos al formulario
            this.lLogo.setIcon(ajustada);

		// si no hay imagen
		} else {

			// fijamos la imagen
			this.lLogo.setIcon(new ImageIcon(getClass().getResource("/Graficos/sin_imagen.jpg")));

		}

		// actualiza el switch
		this.sinModificar();

		// setea el foco
		this.tNombre.requestFocus();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que abre el cuadro de diálogo de búsqueda
     * y luego presenta la grilla de resultados
     */
    protected void buscaLaboratorio(){

		// si el registro fue modificado
		if (this.Modificado){

			// pide confirmación
			if (!this.confirmaAbandona()){
				return;
			}

		}

		// pide el texto a buscar
		String respuesta = JOptionPane.showInputDialog(this,
								  	  "Ingrese el laboratorio a buscar",
									  "Buscar",
									  JOptionPane.INFORMATION_MESSAGE);

		// si canceló
		if (respuesta.equals("")){
			return;
		}

		// busca en la base
		ResultSet nomina = this.Instituciones.buscaLaboratorio(respuesta);

		try {

			// si hubo registros
			if (nomina.next()){

				// instanciamos el formulario y cargamos el resultado
				FormBuscaLaboratorio Encontrados = new FormBuscaLaboratorio(this, true, this);
				Encontrados.cargaLaboratorios(nomina);
				Encontrados.setVisible(true);

			// si no hubo registros
			} else {

				// presentamos el alerta
				JOptionPane.showMessageDialog(this,
											"No se han encontrado laboratorios",
											"Error",
											JOptionPane.ERROR_MESSAGE);
				return;

			}

		// si hubo un error
		} catch (SQLException ex) {

			// presenta el mensaje
			System.out.println(ex.getMessage());

		}


    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar sobre la imagen
     */
    private void lLogoPulsado(java.awt.event.MouseEvent evt) {

        // obtenemos el archivo
        Imagenes foto = new Imagenes();
        foto.leerImagen();

        // si seleccionó un archivo
        if (!foto.getArchivo().isEmpty()){

			// actualizamos el registro modificado
			this.registroModificado();

            // lo asignamos al label redimensionando la imagen
            ImageIcon imagen = foto.cargarImagen(110,110);
            this.lLogo.setIcon(imagen);

            // asignamos la ruta de la imagen
            this.Archivo = foto.getArchivo();

            // obtenemos el archivo y sus propiedades
            foto.obtenerImagen();

            // asignamos en las variables de clase
            this.Contenido = foto.getContenido();
            this.Longitud = foto.getLongitud();

        // si canceló
        } else {

            // inicializamos la variable
            this.Archivo = "";

        }

    }

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que cambia el switch de registro modificado y
	 * actualiza el título de la ventana
	 */
	protected void registroModificado(){

		// si está sin modificar
		if (!this.Modificado){

			// actualiza el switch y cambia el título
			this.Modificado = true;
			this.setTitle("Laboratorios Registrados * Registro Modificado");

		}

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que cambia el switch de registro modificado
	 * cuando se carga o inicializa el formulario
	 */
	protected void sinModificar(){

		// actualiza el switch y cambia el título
		this.Modificado = false;
		this.setTitle("Laboratorios Registrados");

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @return boolean el resultado de la confirmación
	 * Método llamado al estar el registro modificado, pide
	 * confirmación antes de abandonar
	 */
	protected boolean confirmaAbandona(){

		// pide confirmación
		String mensaje = "El registro ha sido modificado, \n";
		mensaje += "se perderán los cambios";
		int seleccion = JOptionPane.showOptionDialog(
									this,
									mensaje,
									"Confirme por favor",
									JOptionPane.YES_NO_CANCEL_OPTION,
									JOptionPane.QUESTION_MESSAGE,
									null,
									new Object[] { "Aceptar", "Cancelar"},
									"Aceptar");

		// si pulsó aceptar (primera opción)
		if (seleccion == 0){
			return true;
		} else {
			return false;
		}

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al pulsar el botón mapa que muestra
	 * en un diálogo emergente el mapa de la zona
	 */
	protected void verMapa(){

		// verificamos si seleccionó país, localidad o provincia y dirección
		if (this.tCoordenadas.getText().equals("")){

			// presenta el mensaje y retorna
			JOptionPane.showMessageDialog(this,
						"No hay declaradas coordenadas válidas",
						"Error",
						JOptionPane.ERROR_MESSAGE);

		// si está cargado
		} else {

			// componemos la dirección
			String Coordenadas = this.tDomicilio.getText() + ", " + this.tJurisdiccion.getText() + ", " + this.tPais.getText();

			// instancia el formulario, carga el mapa y lo muestra
			FormMapas Mapa = new FormMapas(this, true);
			Mapa.setCoordenadas(Coordenadas);
			Mapa.mostrarMapa();
			Mapa.setVisible(true);

		}

	}

    // evento llamado al pulsar el botón salir
    protected void Salir(){

        // verifica si el registro está modificado
        if(this.Modificado){

            // pide confirmación
            if (this.confirmaAbandona()){

            }

        // si no hay cambios
        } else {

            // descartamos el formulario
            this.dispose();

        }

    }

}
