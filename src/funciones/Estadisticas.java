/*

    Nombre: Estadisticas
    Fecha: 22/10/2018
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Herramientas estadisticas

 */

// definición del paquete
package funciones;

// importamos las librerías
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

// definición de la clase
public class Estadisticas {

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param matriz un array multidimensional a buscar
     * @param texto  cadena con el texto a buscar
     * @return entero con el index del elemento Metodo que recibe como parametro una
     *         matriz multidimensional y busca la ocurrencia de esa cadena en la
     *         primera columna de la matriz, si la encuentra retorna el indice del
     *         elemento o -1 en caso de no haberla encontrado
     */
    public int buscaMatriz(String[][] matriz, String texto) {

        // declaracion de variables
        int Fila = -1;

        // recorremos el vector, le restamos uno a la longitud porque
        // la longitud es el número de elementos pero la matriz
        // empieza a contar desde cero
        for (int i = 0; i < matriz.length - 1; i++) {

            // comparamos
            if (matriz[i][0].equals(texto)) {

                // asigna
                Fila = i;
                return Fila;

            }

        }

        // retornamos la fila
        return Fila;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param mayor un entero
     * @param menor un entero
     * @return numeroConFormato Método que recibe dos enteros, en el cual el primero
     *         es el denominador y el segundo el numerador y retorna el porcentaje
     *         formateado
     */
    public String Porcentaje(int mayor, int menor) {

        // definición de variables
        String numeroConFormato;
        DecimalFormat formatoPorcentaje;
        float Proporcion;

        // definimos la máscara de porcentaje
        formatoPorcentaje = new DecimalFormat("0.00%");

        // calculamos el porcentaje
        Proporcion = (float) menor / (float) mayor;

        // formateamos el número
        numeroConFormato = formatoPorcentaje.format(Proporcion);

        // retornamos el número formateado
        return numeroConFormato;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param numero un número con varias posiciones decimales
     * @return numero con formato, un string Método que recibe como parámetro un
     *         número con varias posiciones decimales formatea el número a dos
     *         decimales y lo retorna como cadena
     */
    public String formatoNumero(float numero) {

        // declaración de variables
        String numeroConFormato;
        DecimalFormat formatoDecimal;
        DecimalFormatSymbols simbolos;

        // fijamos el punto decimal como separador porque luego con las funciones
        // estadísticas parsefloat solo reconoce el punto
        simbolos = new DecimalFormatSymbols();
        simbolos.setDecimalSeparator('.');

        // definimos la máscara
        formatoDecimal = new DecimalFormat("0.00", simbolos);

        // verificamos haber recibido un numero
        if (Float.isNaN(numero)) {
            numeroConFormato = "0.00";
        } else {
            // formateamos el número
            numeroConFormato = formatoDecimal.format(numero);
        }

        // lo retornamos
        return numeroConFormato;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param Matriz  array multidimensional
     * @param Columna entero indicando la columna a calcular
     * @return cadena con la media calculada Método que recibe como parámetro una
     *         matriz multidimensional y la columna a calcular, obtiene entonces la
     *         media de esa columna, la formatea a dos decimales y retorna
     */
    public String calculaMedia(String[][] Matriz, int Columna) {

        // declaración de variables
        String media;
        float Sumatoria = 0;

        // recorremos el vector sumando los valores
        for (int i = 0; i < Matriz.length; i++) {

            // sumamos verificando el valor
            if (Matriz[i][Columna] != null) {

                // verificamos de poder convertirlo
                try {

                    // lo sumamos
                    Float Flotante = Float.parseFloat(Matriz[i][Columna]);
                    Sumatoria += Flotante;

                    // si no pudo convertirlo
                } catch (NumberFormatException ex) {
                    ex.printStackTrace();
                }

            }

        }

        // calculamos la media y la formateamos
        float promedio = Sumatoria / Matriz.length;
        media = this.formatoNumero(promedio);

        // retornamos la media
        return media;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param Matriz  array multidimensional
     * @param Columna entero indicando la columna a calcular
     * @return cadena con el desvío estandar de la muestra Método que recibe como
     *         parámetro una matriz, la columna con los datos a calcular y retorna
     *         una cadena formateada con el desvío estandar de esa columna
     */
    public String calculaDesvio(String[][] Matriz, int Columna) {

        // la técnica utilizada es calcular la sumatoria de los cuadrados
        // de las distancias con respecto a la media de cada uno de los
        // valores y luego dividirlos por el número de elementos menos 1
        // y calcular la raíz cuadrada

        // declaración de variables
        String Desvio;
        float Media;
        double CuadDistancias = 0;
        int Longitud = Matriz.length;

        // primero obtenemos la media de la muestra
        Media = Float.parseFloat(this.calculaMedia(Matriz, Columna));

        // ahora recorremos la matriz sumando el cuadrado de las distancias
        for (int i = 0; i < Longitud; i++) {

            // obtenemos la distancia con la media verificando que
            // el valor no sea nulo
            String Valor = Matriz[i][Columna];
            if (Valor != null) {

                // obtenemos la distancia
                float distancia = Media - Float.parseFloat(Matriz[i][Columna]);

                // sumamos el cuadrado de la distancia
                CuadDistancias += Math.pow(distancia, 2);

            }

        }

        // ahora lo dividimos por el número de elementos menos 1
        CuadDistancias = CuadDistancias / (Longitud - 1);

        // obtenemos la raiz
        CuadDistancias = Math.sqrt(CuadDistancias);

        // lo formateamos
        Desvio = this.formatoNumero((float) CuadDistancias);

        // retornamos el desvío
        return Desvio;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param nroMuestras entero con la longitud de la muestra
     * @param desvio      string con el desvío de la muestra
     * @return string con el error Método que calcula el error standar de una
     *         muestra simplemente dividiendo el desvío por el número de elementos
     *         de la muestra
     */
    public String calculaError(int nroMuestras, String desvio) {

        // declaración de variables
        String Error = "";

        // calculamos
        float errorTemp = Float.parseFloat(desvio) / nroMuestras;

        // lo formateamos
        Error = this.formatoNumero(errorTemp);

        // retornamos el error
        return Error;

    }

}